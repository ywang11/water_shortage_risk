"""
2021/3/23
ywang254@utk.edu

The R-square and five most important predictors in each province.
"""
import pandas as pd
import numpy as np
import os
from utils.paths import path_data, path_out
from utils.constants import *


# Selected hyperparameters of the Random Forest model, based on the results
# of "regression_both_test6.py" and "regression_both_test6_part1_plot.py"
# Restrict the number of predictors to >= 1
params = {'wd': {'n_estim': 400, 'n_features': 15,
                 'n_split': 'max_max'},
          'wsi': {'n_estim': 100, 'n_features': 5,
                  'n_split': 'max_max'}}


performance = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                       'part2', 'performance.csv'),
                          index_col = 0, header = [0,1])


#
npred = 5
f = pd.ExcelWriter(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                'analyze', 'table1.xlsx'),
                   engine = 'openpyxl')
for var in ['wsi', 'wd']:
    table = pd.DataFrame(np.nan,
                         index = province,
                         columns = ['R2'] + \
                         ['Predictor ' + str(nn+1) for nn in range(npred)])

    table.loc[:, 'R2'] = performance.loc[:, (var, 'R2')]
    # skip provinces without significant p-values
    temp = (performance.loc[:, (var, 'pVal')] > 0.05) | \
        (performance.loc[:, (var, 'R2')] < 0)
    table.loc[temp, 'R2'] = np.nan
    province_subset = performance.index[~temp]

    for pr in province_subset:
        importance = pd.read_csv(os.path.join(path_out, 'regression',
                                              'wsi_wd_test6',
                                              'part1', var + '_norm_' + \
                                              'importance_' + pr + '.csv'),
                                 header = [0,1,2],
                                 index_col = [0,1,2])
        temp = importance.loc[(params[var]['n_features'],
                               params[var]['n_split'],
                               params[var]['n_estim']), :]
        temp = temp[~np.isnan(temp)]
        ind = np.argsort(temp.values)[::-1] # descending order

        for nn in range(npred):
            table.loc[pr, 'Predictor ' + str(nn+1)] = \
                '-'.join(temp.index[ind[nn]])

    table.to_excel(f, sheet_name = var)
f.close()
