import os
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os
import numpy as np
import seaborn as sns
from statannotations.Annotator import Annotator
import matplotlib.ticker as ticker
from utils.paths import *


path_temp = os.path.join(path_out, 'risk_results', '20220505', 'Raw data for Fig. 4~9', '01 original data for the boxplots (fig4~7)')


rcParams['axes.titlesize'] = 6
rcParams['font.size'] = 6
lab = 'abcd'


region_group = {'North': ['Beijing', 'Tianjin', 'Hebei', 'Shanxi', 'Inner Mongolia'],
                'Northeast': ['Liaoning', 'Jilin', 'Heilongjiang'],
                'East': ['Shanghai', 'Jiangsu', 'Zhejiang', 'Anhui', 'Fujian', 'Jiangxi', 'Shandong'],
                'Central': ['Henan', 'Hubei', 'Hunan', 'Guangdong', 'Guangxi', 'Hainan'],
                'Southwest': ['Chongqing', 'Sichuan', 'Guizhou', 'Yunnan'],
                'Northwest': ['Shaanxi', 'Gansu', 'Qinghai', 'Ningxia', 'Xinjiang']}
region_group_reverse = dict([(c, a) for a, b in region_group.items() for c in b])


diff_median_minmax = pd.DataFrame(np.nan, columns = ['lwsr_diff_chang_prov', 'vwsr_diff_chang_prov', 
                                                     'lwsr_diff_chang_sector', 'vwsr_diff_chang_sector'],
                                  index = pd.MultiIndex.from_product([['SVD1', 'SVD2'], ['min', 'max']]))


# 
for file in ['lwsr_diff_chang_prov', 'vwsr_diff_chang_prov', 'lwsr_diff_chang_sector', 'vwsr_diff_chang_sector']:
    data = pd.read_csv(os.path.join(path_temp, f'{file}.csv'))

    data = data.rename({'diff_perc': 'Percentage gaps (%)'}, axis = 1)
    data['SVD'] = data['SVDs'].apply(lambda x: x[:-1])
    data['Phase'] = data['SVDs'].apply(lambda x: x[-1]).map({'+': '+', '-': '\u2212'})

    if 'prov' in file:
        data['region_group'] = data['region'].map(region_group_reverse)
        region_list = region_group['North'] + region_group['Northeast'] + region_group['East'] + region_group['Central'] + region_group['Southwest'] + region_group['Northwest']
    else:
        region_list = ['Ag', 'CoalM', 'PetroGas', 'MetalM', 'NonM', 'Food', 'Tex', 'Cloth', 'Wood', 'PaperM', 'PetroRe',
                       'CheInd', 'NonP', 'Metallurgy', 'MetalP', 'GeSpe', 'TransE', 'ElectriEqui',
                       'EltectroEqui', 'Instru', 'OthManu', 'EleWaSu', 'GasWaSu', 'Con', 'TransS',
                       'WholR', 'Hot', 'LeaComS', 'SciR', 'OthSe']

    setup = dict(
        x = 'region',
        y = 'Percentage gaps (%)',
        order = region_list,
        hue = 'Phase', 
        hue_order = ['+', '\u2212'],
        palette = {'+': '#6a51a3', '\u2212': '#41ab5d'}
    )

    g = sns.catplot(**setup, col = 'SVD', col_order = ['SVD 1', 'SVD 2'], col_wrap = 1, kind = 'box', data = data, estimator = 'median', 
                    linewidth = 0.5, fliersize = 2.5, whis = [5, 95], flierprops = {'markeredgecolor': 'none', 'marker': 'o', 'markerfacecolor': 'k'},
                    height = 2.5, aspect = 3, legend_out = False)
    g.set_titles("{col_name}")
    sns.move_legend(g, 'upper right', ncol = 2, bbox_to_anchor=(.8, 0.985))

    for i, ax in enumerate(g.axes):
        # ax.set_xticklabels(ax.get_xticklabels(), rotation = 90) # , fontdict = {'horizontalalignment': 'right'})
        ax.set_xticklabels([])
        ax.set_xlabel('')
        ax.axhline(0, ls = ':', color = 'k', lw = 0.5)

        annotator = Annotator(ax, [((r, '+'), (r, '\u2212')) for r in setup['order']],
                              data = data.loc[data['SVD'] == f'SVD {i+1}', :], **setup)
        annotator.configure(test='Mann-Whitney', text_format = 'star', line_width = 0.5, line_offset = -10, color = 'b')
        annotator.apply_and_annotate()
        for t in ax.texts:
            if t.get_text() == 'ns':
                t.set_color('b')
            else:
                t.set_color('r')

        ax.text(0.01, 0.95, lab[i], transform = ax.transAxes, fontdict = {'weight': 'bold'})

        if file == 'lwsr_diff_chang_prov':
            ax.set_yscale('symlog', linthresh = 50)
            ax.set_yticks([-100, -50, -25, 0, 25, 50, 100, 500, 1000, 5000, 10000])
            ax.set_yticklabels([-100, -50, -25, 0, 25, 50, 100, 500, 1000, 5000, 10000])
        else:
            ax.set_yscale('symlog', linthresh = 10)
            ax.set_yticks([-100, -50, -10, -5, 0, 5, 10, 50, 100])
            ax.set_yticklabels([-100, -50, -10, -5, 0, 5, 10, 50, 100])

        #mean_vals = data.loc[data['SVD'] == f'SVD {i+1}', :].groupby(['region', 'Phase']).mean()
        #mean_vals = mean_vals.loc[region_list]
        #ax.scatter(np.arange(0, len(mean_vals) / 2, 0.5) - 0.25, mean_vals, marker = 'o', s = 6, color = 'r')

        ax.spines['top'].set(visible = False)
        ax.spines['right'].set(visible = False)

        """
        if 'prov' in file and i == 1:
            ax2 = ax.twiny()
            ax2.spines['top'].set(visible = False)
            ax2.spines['right'].set(visible = False)
            ax2.spines['bottom'].set_position(('axes', - 0.45))
            ax2.tick_params('both', length = 0., width = 0., which = 'minor')
            ax2.tick_params('both', direction = 'in', which = 'major')
            ax2.xaxis.set_ticks_position('bottom')
            ax2.xaxis.set_label_position('bottom')

            xticks = [0]
            for r, l in region_group.items():
                xticks.append(xticks[-1] + len(l))
            xticks = np.array(xticks)
            xticks_mid = (xticks[1:] + xticks[:-1]) * 0.5

            ax2.set_xticks(xticks)
            ax2.xaxis.set_major_formatter(ticker.NullFormatter())
            ax2.xaxis.set_minor_locator(ticker.FixedLocator(xticks_mid))
            ax2.xaxis.set_minor_formatter(ticker.FixedFormatter(list(region_group.keys())))
        """

    # data_mean = data.groupby(['region', 'SVDs'])['Percentage gaps (%)'].mean().unstack().loc[region_list, :]
    data_median = data.groupby(['region', 'SVDs'])['Percentage gaps (%)'].median().unstack().loc[region_list, :]

    bbox = g.axes[0].get_position()

    fig = g.fig
    axes = np.empty(2, dtype = object)
    axes[0] = fig.add_axes([bbox.x0, -0.385, bbox.x1 - bbox.x0, 0.38])
    axes[1] = fig.add_axes([bbox.x0, -0.835, bbox.x1 - bbox.x0, 0.38])

    for i in range(2):
        # diff_mean = data_mean[f'SVD {i+1}+'] - data_mean[f'SVD {i+1}-'].values
        diff_median = data_median[f'SVD {i+1}+'] - data_median[f'SVD {i+1}-'].values

        diff_median_minmax.loc[(f'SVD{i+1}', 'min'), file] = diff_median.min()
        diff_median_minmax.loc[(f'SVD{i+1}', 'max'), file] = diff_median.max()

        ax = axes.flat[i]
        # ax.plot(range(len(diff_mean)), diff_mean.values, '-r', label = 'mean')
        ax.plot(range(len(diff_median)), diff_median.values, '-ob', label = 'median')
        ax.set_xlim([-0.5, len(diff_median) - 0.5])

        if i == 1 and file == 'lwsr_diff_chang_sector':
            ax.text(0.05, 0.95, lab[i+2], transform = ax.transAxes, fontdict = {'weight': 'bold'})
        else:
            ax.text(0.01, 0.95, lab[i+2], transform = ax.transAxes, fontdict = {'weight': 'bold'})
        ax.set_title(f'SVD {i+1}')

        ax.set_xticks(range(len(diff_median)))
        if i == 0:
            ax.set_xticklabels([])
        else:
            ax.set_xticklabels(diff_median.index, rotation = 90)
        if i == 0 and file == 'lwsr_diff_chang_prov':
            ax.set_yscale('symlog', linthresh = 10)
        ax.set_ylabel('$\Delta$' + '+\u2212 percentage gaps (%)')
        ax.axhline(0, ls = ':', color = 'k', lw = 0.5)

        # if i == 0:
        #     ax.legend(ncol = 2, loc = 'best')

        if 'prov' in file and i == 1:
            ax2 = ax.twiny()
            ax2.spines['top'].set(visible = False)
            ax2.spines['right'].set(visible = False)
            ax2.spines['bottom'].set_position(('axes', - 0.45))
            ax2.tick_params('both', length = 0., width = 0., which = 'minor')
            ax2.tick_params('both', direction = 'in', which = 'major')
            ax2.xaxis.set_ticks_position('bottom')
            ax2.xaxis.set_label_position('bottom')

            xticks = [0]
            for r, l in region_group.items():
                xticks.append(xticks[-1] + len(l))
            xticks = np.array(xticks)
            xticks_mid = (xticks[1:] + xticks[:-1]) * 0.5

            ax2.set_xticks(xticks)
            ax2.xaxis.set_major_formatter(ticker.NullFormatter())
            ax2.xaxis.set_minor_locator(ticker.FixedLocator(xticks_mid))
            ax2.xaxis.set_minor_formatter(ticker.FixedFormatter(list(region_group.keys())))

    g.savefig(os.path.join(path_temp, f'{file}.png'), dpi = 600., bbox_inches = 'tight')


diff_median_minmax.to_csv(os.path.join(path_temp, 'diff_median_minmax.csv'))