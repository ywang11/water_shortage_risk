"""
20200503
ywang254@utk.edu

Plot the regression results. Compare between the different hyperparameters.
"""
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.cm import get_cmap
from utils.paths import path_data, path_out
from utils.constants import *
import itertools as it


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
cmap = get_cmap('Spectral')
clist = [cmap((i+0.5)/(len(province)+1)) for i in range(len(province))]


#
r2_collect = {}
for prv in province:
    r2_collect[prv] = pd.read_csv(os.path.join(path_out, 'regression',
                                               'wsi_wd_test6', 'part1',
                                               'wd_r2_' + prv + '.csv'),
                                  index_col = [0,1,2], header = 0)
r2_collect = pd.concat(r2_collect, axis = 1)
r2_collect.columns = r2_collect.columns.reorder_levels([1,0])
r2_collect = r2_collect.sort_index(axis = 1)


#
n_estim_list = r2_collect.index.levels[2]
n_features_list = r2_collect.index.levels[0]
n_split_list = r2_collect.index.levels[1]


#
for m, which in it.product(range(len(n_split_list)),
                           ['XV Average', 'Test']):
    fig, axes = plt.subplots(nrows = len(n_features_list),
                             ncols = len(n_estim_list),
                             figsize = (6.5, 12),
                             sharex = False, sharey = True)
    fig.subplots_adjust(wspace = 0.07, hspace = 0.12)
    hb_list = []
    for i,k in it.product(range(len(n_features_list)),
                          range(len(n_estim_list))):
        ax = axes[i,k]

        temp = r2_collect.loc[(n_features_list[i], n_split_list[m],
                               n_estim_list[k]), which]
        hb = ax.bar(range(len(province)), temp.loc[province],
                    color = clist)
        ax.axhline(y = 0, linestyle = '--', color = 'k', linewidth = 0.5,
                   zorder = 2)
        ax.axhline(y = np.mean(temp),
                   linestyle = '-', color = 'b', linewidth = 0.5)
        if (i == 0) & (k == 2):
            ax.text(0.5, 1.5,
                    'R-square ' + which + '; Split = ' + n_split_list[m],
                    transform = ax.transAxes,
                    horizontalalignment = 'center')
        if k == 0:
            ax.set_ylabel('n_features = ' + str(n_features_list[i]))
        if i == 0:
            ax.set_title('n_estim = ' + str(n_estim_list[k]))
        ax.set_ylim([-1, 1])
        ax.set_xticks(range(len(province)))
        if i == len(n_features_list)-1:
            ax.set_xticklabels(province, rotation = 90)
        else:
            plt.setp(ax.get_xticklabels(), visible = False)
    fig.savefig(os.path.join(path_out, 'regression',
                             'wsi_wd_test6', 'part1',
                             'wd_r2_' + n_split_list[m] + \
                             '_' + which + '.png'), dpi = 600,
                bbox_inches = 'tight')
    plt.close(fig)
