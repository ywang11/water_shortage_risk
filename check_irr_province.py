"""
20200202
ywang254@utk.edu

The amount of irrigated crop land area in each province. Check the variables. 

cropland_total = fractional land area = cropland_irrigated + cropland_rainfed

https://www.isimip.org/gettingstarted/input-data-bias-correction/details/22/
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils.paths import path_data, path_out


hr = xr.open_dataset(os.path.join(path_data, 'histsoc_landuse-totals_'
                                  'annual_1861_2005.nc'), decode_times = False)
base = pd.to_datetime('1700-01-01')
hr['time'] = pd.to_datetime([str(1700+int(x)) + '-01-01' for x in \
                             hr['time'].values])

cropland = hr['cropland_total'].loc[hr['time'].to_index().year >= 1971,
                                    :, :].copy(deep = True).load()
irr = hr['cropland_irrigated'].loc[hr['time'].to_index().year >= 1971,
                                   :, :].copy(deep = True).load()
rfd = hr['cropland_rainfed'].loc[hr['time'].to_index().year >= 1971,
                                 :, :].copy(deep = True).load()
pasture = hr['pastures'].loc[hr['time'].to_index().year >= 1971,
                             :, :].copy(deep = True).load()
# All-zero. Meaning unclear.
#lutot = hr['landuse-totals'].loc[hr['time'].to_index().year >= 1971,
#                                 :, :].copy(deep = True).load()

hr.close()


print(np.nanmax(cropland))
print(np.nanmin(cropland))
print(np.nanmax(irr))
print(np.nanmin(irr))
print(np.nanmax(rfd))
print(np.nanmin(rfd))
print(np.nanmax(pasture))
print(np.nanmin(pasture))

print(np.nanmax(cropland - irr - rfd))
print(np.nanmin(cropland - irr - rfd))
