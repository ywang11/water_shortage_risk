"""
20200202
ywang254@utk.edu

Organize the climate indices.
"""
import pandas as pd
import numpy as np
import os
from utils.paths import path_data, path_out
from utils.constants import *


inx = pd.DataFrame(np.nan, index = range(1901, 2016),
                   columns = pd.MultiIndex.from_product( \
                       [xclim, ['Annual','DJF','MAM','JJA','SON']]))


###### AMO
df = pd.read_csv(os.path.join(path_data, 'AMO.dat'), sep = '\s+',
                 header = None, index_col = 0).stack()
df.index = pd.date_range('1856-01-01', '2015-12-31',
                         freq = 'MS').to_period('Q-NOV')
df = df.groupby(df.index).mean().iloc[1:(-1)] # First & last Q1 incomplete
df = df.loc[(df.index.year >= 1901) & (df.index.year <= 2015)]
for qtr,ss in zip([1,2,3,4], ['DJF','MAM','JJA','SON']):
    inx.loc[:, ('AMO',ss)] = df.loc[df.index.quarter == qtr].values
inx.loc[:, ('AMO','Annual')] = df.groupby(df.index.year).mean()


###### idmi
df = pd.read_csv(os.path.join(path_data, 'idmi.dat'), sep = '\s+',
                 skiprows = 3, header = None, index_col = [0,1])
df.index = pd.date_range('1870-01-01', '2018-07-31',
                         freq = 'MS').to_period('Q-NOV')
df = df.groupby(df.index).mean().iloc[1:(-1)] # First & last Q1 incomplete
df = df.loc[(df.index.year >= 1901) & (df.index.year <= 2015)]
for qtr,ss in zip([1,2,3,4], ['DJF','MAM','JJA','SON']):
    inx.loc[:, ('IDMI',ss)] = df.loc[df.index.quarter == qtr].values
inx.loc[:, ('IDMI','Annual')] = df.groupby(df.index.year).mean()


###### NAO
df = pd.read_csv(os.path.join(path_data, 'nao_station_seasonal.txt'),
                 sep = '\s+', skiprows = 1, index_col = 0)
df = df.loc[1901:2015, :]
for qtr,ss in zip([1,2,3,4], ['DJF','MAM','JJA','SON']):
    inx.loc[:, ('NAO',ss)] = df.loc[:, ss]
inx.loc[:, ('NAO','Annual')] = df.loc[:, ['DJF','MAM','JJA',
                                          'SON']].mean(axis=1)


###### PDO
df = pd.read_csv(os.path.join(path_data, 'PDO.dat'), sep = '\s+',
                 header = 0, index_col = 0).stack()
df.index = pd.date_range('1900-01-01', '2015-12-31',
                         freq = 'MS').to_period('Q-NOV')
df = df.groupby(df.index).mean().iloc[1:(-1)] # First & last Q1 incomplete
df = df.loc[(df.index.year >= 1901) & (df.index.year <= 2015)]
for qtr,ss in zip([1,2,3,4], ['DJF','MAM','JJA','SON']):
    inx.loc[:, ('PDO',ss)] = df.loc[df.index.quarter == qtr].values
inx.loc[:, ('PDO','Annual')] = df.groupby(df.index.year).mean()


###### SOI
df = pd.read_csv(os.path.join(path_data, 'SOI.dat'), sep = '\s+',
                 header = 0, index_col = 0).stack()
df.index = pd.date_range('1876-01-01', '2015-12-31',
                         freq = 'MS').to_period('Q-NOV')
df = df.groupby(df.index).mean().iloc[1:(-1)] # First & last Q1 incomplete
df = df.loc[(df.index.year >= 1901) & (df.index.year <= 2015)]
for qtr,ss in zip([1,2,3,4], ['DJF','MAM','JJA','SON']):
    inx.loc[:, ('SOI',ss)] = df.loc[df.index.quarter == qtr].values
inx.loc[:, ('SOI','Annual')] = df.groupby(df.index.year).mean()

    
#####
inx.to_csv(os.path.join(path_out, 'inx.csv'))
