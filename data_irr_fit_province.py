"""
20200202
ywang254@utk.edu

The amount of irrigated crop land area in each province. 

irr_area = area of grid cell * cropland_irrigated

unit: km2

cropland_total = fractional land area = cropland_irrigated + cropland_rainfed

https://www.isimip.org/gettingstarted/input-data-bias-correction/details/22/
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils.paths import path_data, path_out
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


hr = xr.open_dataset(os.path.join(path_data, 'histsoc_landuse-totals_'
                                  'annual_1861_2005.nc'),
                     decode_times = False)
base = pd.to_datetime('1700-01-01')
hr['time'] = pd.to_datetime([str(1700+int(x)) + '-01-01' for x in \
                             hr['time'].values])
irr = hr['cropland_irrigated'].loc[hr['time'].to_index().year >= 1971,
                                   ::-1, :].copy(deep = True).load() # S->N
hr.close()


hr = xr.open_dataset(os.path.join(path_data, 'elm_half_degree_grid_negLon.nc'))
area = hr['area'].values.copy()
hr.close()


irr_area = irr * area

fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()},
                       figsize = (10, 10))
#ax.contourf(irr_area.lon, irr_area.lat, irr_area.mean(dim = 'time'),
#            transform = ccrs.PlateCarree())
irr_area.mean(dim = 'time').plot()
fig.savefig(os.path.join(path_out, 'irr_area.png'), dpi = 600.)


province = ['Anhui', 'Beijing', 'Fujian', 'Gansu',
            'Guangdong', 'Guangxi', 'Guizhou', 'Hainan',
            'Hebei', 'Henan', 'Heilongjiang', 'Hubei',
            'Hunan', 'Jilin', 'Jiangsu', 'Jiangxi',
            'Liaoning', 'Inner Mongolia', 'Ningxia',
            'Qinghai', 'Shandong', 'Shanxi', 'Shaanxi',
            'Shanghai', 'Sichuan', 'Tianjin', 'Tibet',
            'Xinjiang', 'Yunnan', 'Zhejiang', 'Chongqing']
prv_ia = pd.DataFrame(np.nan, index = irr_area['time'].to_index(),
                      columns = province)
prv = xr.open_dataset(os.path.join(path_out, 'CHN_mask_0.5.nc'))
map_keys = dict([(x.split(': ')[1], int(x.split(': ')[0])) \
                 for x in prv.attrs['Key'].split('; ')])
for p in province:
    prv_ia.loc[:, p] = irr_area.where(prv.region == map_keys[p] \
    ).sum(dim = ['lat', 'lon'])
prv_ia.to_csv(os.path.join(path_out, 'prv_irr_area.csv'))
prv.close()
