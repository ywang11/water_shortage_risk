"""
2021/03/14
ywang254@utk.edu
"""
import os
import numpy as np
import pandas as pd
from utils.constants import *
from utils.paths import *
import matplotlib.pyplot as plt
import matplotlib as mpl
from utils.plotting import plot_province
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature
from utils.plotting import ax_histogram
import itertools as it


def read_data(var):
    goal = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                    'part2', 'goal_' + var + '_bc.csv'),
                       index_col = 0)

    restored = {}
    for nn in range(npcs + 1):
        restored[nn] = pd.read_csv(os.path.join(path_out, 'regression',
                                                'wsi_wd_test6',
                                                'part4',
                                                'restored_' + var + '_' + \
                                                str(nn) + '_bc.csv'),
                                   index_col = 0)

    le = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 
                                  'part3', 'multivar_le.csv'),
                     parse_dates = True, 
                     index_col = 0, header = [0,1]).loc[:, (slice(None), '0')]
    le.columns = le.columns.droplevel(1)
    le = le.groupby(le.index.year, axis = 0).mean().loc[1920:, :]
    le_signs = {}
    for nn in range(npcs + 1):
        le_signs[(nn, 'positive')] = le.index[ le[str(nn)] > 0. ]
        le_signs[(nn, 'negative')] = le.index[ le[str(nn)] < 0. ]
    return goal, restored, le_signs


mpl.rcParams['font.size'] = 6.
mpl.rcParams['axes.titlesize'] = 6.
mpl.rcParams['axes.titleweight'] = 'bold'


npcs = 1
for var in ['wsi', 'wd']:
    goal, restored, le_signs = read_data(var)

    # pdf plot
    fig, axes = plt.subplots(6, 5, figsize = (12., 12.),
                             sharex = False, sharey = False)
    fig.subplots_adjust(wspace = 0.35, hspace = 0.35)
    h2 = []
    h3 = []
    for pind, pr in enumerate(province):
        ax = axes.flat[pind]

        if np.std(goal.loc[:, pr].values) < 1e-8:
            ax.bar(goal.loc[:, pr].values[0], 10, 
                   color = '#cbc9e2', edgecolor = '#756bb1')
            ax.set_xlim([goal.loc[:, pr].values[0] - 2,
                         goal.loc[:, pr].values[0] + 2])
        else:
            h1 = ax_histogram(ax, goal.loc[:, pr].values,
                              goal.shape[0] // 10, dist = 'norm', show = 'both', 
                              args_hist = {'color': '#cbc9e2',
                                           'edgecolor': '#756bb1',
                                           'alpha': 0.3},
                              args_line = {'color': '#756bb1'})

        clist = [['#fb6a4a', '#de2d26', '#a50f15'],
                 ['#74c476', '#31a354', '#006d2c']]

        for nn in range(npcs + 1):
            if np.std(restored[nn].loc[:, pr].values) < 1e-8:
                ax.bar(restored[nn].loc[:, pr].values[0], 10,
                       color = clist[nn][0], edgecolor = clist[nn][1])
                ax.set_xlim([restored[nn].loc[:, pr].values[0] - 2,
                             restored[nn].loc[:, pr].values[0] + 2])
            else:
                htemp = ax_histogram(ax, restored[nn].loc[:, pr].values,
                                     goal.shape[0] // 10, dist = 'norm', show = 'line',
                                     args_hist = {'color': clist[nn][0],
                                                  'edgecolor': clist[nn][1],
                                                  'alpha': 0.3},
                                     args_line = {'color': clist[nn][2]})
                if pind == 0:
                    #h2.append(htemp[0])
                    #h3.append(htemp[1])
                    h2.append(htemp[0])

                for mm,ss,ls in zip(range(2), ['positive', 'negative'], ['--',':']):
                    htemp = ax_histogram(ax, goal.loc[le_signs[(nn, ss)], pr].values,
                                         goal.shape[0] // 10, dist = 'norm', show = 'line',
                                         args_hist = {'color': clist[nn][0],
                                                      'edgecolor': clist[nn][1],
                                                      'alpha': 0.3, 'ls': ls},
                                         args_line = {'color': clist[nn][1],
                                                      'ls': ls})
                    if pind == 0:
                        #h2.append(htemp[0])
                        #h3.append(htemp[1])
                        h2.append(htemp[0])
        #h4 = ax.axvline(goal.loc[2012, pr], color = 'k')
        h4 = ax.scatter(goal.loc[2012, pr], ax.get_ylim()[1]/2,
                        marker = '^', color = 'k', zorder = 3)

        ax.set_title(pr, pad = 2)
        ax.tick_params(axis = 'both', length = 2)

    ax.legend(h1 + h2 + h3 + [h4],
              ['Histogram Met', 'Fitted Met',
               'Fitted SVD 1', 'Fitted SVD 1+', 'Fitted SVD 1\u2212',
               'Fitted SVD 2', 'Fitted SVD 2+', 'Fitted SVD 2\u2212',
               'Observed 2012 value'], ncol = 5, loc = (-5, -0.5))

    fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                             'analyze', 'figure2_' + var + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)

    # map
    fig, ax = plt.subplots(figsize = (4, 4),
                           subplot_kw = {'projection': ccrs.PlateCarree()})
    if var == 'wsi':
        levels = np.linspace(0., 1., 21)
        cf = plot_province(ax, goal.loc[2012, province].values,
                           cmap = 'rainbow', levels = levels)
    else:
        levels = np.arange(0.01, 0.095, 0.005)
        cf = plot_province(ax, goal.loc[2012, province].values,
                           cmap = 'rainbow', levels = levels, extend = 'both')
    sf = ShapelyFeature(Reader(os.path.join(path_data, 'China_Provinces',
                                            'China-provincial_level.shp')).geometries(),
                        ccrs.PlateCarree(), edgecolor = 'grey', facecolor = 'none',
                        linewidth = 1.5)
    ax.add_feature(sf)
    plt.colorbar(cf, ax = ax, orientation = 'horizontal', aspect = 50)
    fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                             'analyze', 'figure2_' + var + '_map.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)

