"""
Add lag to the meteorological factors based on the lags identified in
 spearman correlation.
"""
from utils.paths import *
from utils.io import *
from utils.constants import *
from utils.plotting import *
import xarray as xr
import pandas as pd
import numpy as np
import os
import itertools as it


#
for var in ['wsi','wd']:
    met_aggr = pd.read_csv(os.path.join(path_out, 'met_province',
                                        'scenario_' + var + '.csv'),
                           index_col = 0, header = [0,1])
    max_sig_lag = pd.read_csv(os.path.join(path_out, 'regression',
                                           var + '_lagged_sig.csv'),
                              index_col = 0, header = [0,1])

    if var == 'wd':
        model = ['cru']

    for pind, mind in it.product(range(len(province)),
                                 range(len(model))):
        m = model[mind]
        pr = province[pind]

        met_collection = pd.DataFrame(data = np.nan, index = met_aggr.index,
                                      columns = [])
        for vind,v in enumerate(met):
            if np.isnan(max_sig_lag.loc[pr, (m,v)]):
                continue
            max_lag = int(max_sig_lag.loc[pr, (m,v)])
            for lag in range(max_lag+1):
                if lag == 0:
                    met_collection.loc[:, v + '_lag_0'] = \
                        met_aggr.loc[:, (v, pr)].values
                else:
                    met_collection.loc[:, v + '_lag_' + str(lag)] = \
                        np.insert(met_aggr.loc[:, (v, pr)].values[:-lag],
                                  0, np.full(lag, np.nan))
        met_collection.to_csv(os.path.join(path_out, 'met_province',
                                           var + '_lagged',
                                           'scenario_' + m + '_' + \
                                           pr + '.csv'))
