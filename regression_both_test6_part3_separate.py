"""
2021/2/28
ywang254@utk.edu

For each meteorological variable, pick the PCs and lags (same lag for all PCs)
 that best explain the variability across the provinces. Minor non-significance
 are ignored.
Remove the explained variance by these PCs.
"""
import os
import numpy as np
from utils.constants import *
from utils.paths import *
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from regression_both_test6_part3_analysis_univariate \
    import get_met_data, get_sst, reshape_sst, restore_sst, match_time
from scipy.stats import linregress
from utils.analysis import deseasonalize_2d, detrend_2d
import xarray as xr


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5


npcs = 1 # Use the first two PCs
lag = 0


le = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                              'part3', 'multivar_le.csv'),
                 header = [0,1]).set_index(('PC','Lag'))
le.index.name = ''
le.columns.names = ['PC','Lag']

re = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                              'part3', 'multivar_re.csv'),
                 header = [0,1]).set_index(('PC','Lag'))
re.index.name = ''
re.columns.names = ['PC','Lag']

hr = xr.open_dataset(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                  'part3', 'multivar_lag_' + str(lag) + \
                                  '_fit.nc'))
rh = hr['rightHomoPatterns'].copy(deep = True)
hr.close()


# the mean and std of un-normalized values.
logpr = True
temp0 = get_met_data(normalize = False, logpr = logpr)
input_met_mean = temp0.mean(axis = 0)
input_met_std = temp0.std(axis = 0)


#
input_met0 = get_met_data(normalize = True, logpr = logpr)
input_met, met_trend = detrend_2d(input_met0)
input_met, met_seasonality = deseasonalize_2d(input_met)


# Try the regression for each lag; the same lag for all the PCs.
f_restored = pd.ExcelWriter(os.path.join(path_out, 'regression',
                                         'wsi_wd_test6', 'part3',
                                         'multivar_separate_restored.xlsx'),
                            engine = 'openpyxl')
coefs = pd.DataFrame(np.nan,
                     index = ['PC' + str(n) for n in range(npcs+1)],
                     columns = ['Coefs', 'pVals'])
for nn in range(npcs+1):
    pattern = pd.DataFrame(rh.sel(n = nn).values,
                           index = rh['var'].values,
                           columns = province).stack()

    X = le.loc[:, (str(nn), str(lag))].values
    y = re.loc[:, (str(nn), str(lag))].values

    result = linregress(X, y)
    coefs.loc['PC'+str(nn), 'Coefs'] = result.slope
    coefs.loc['PC'+str(nn), 'pVals'] = result.pvalue

    restored_met = input_met - \
        np.matmul(result.slope * (X.reshape(-1,1) - np.mean(X)),
                  pattern.loc[input_met.columns].values.reshape(1,-1))

    # The restored values may be negative for PET and P, but can still
    # be input into the random forest model.
    restored_met = (restored_met + met_seasonality + met_trend) * \
        input_met_std + input_met_mean

    if logpr:
        restored_met.loc[:, 'pr'] = np.exp(restored_met.loc[:, 'pr'].values) \
            - 1

    # Save the residuals, restored values, removed components, and
    # regression coefficients to file.
    restored_met.to_excel(f_restored, sheet_name = 'PC' + str(nn))

f_restored.close()
coefs.to_csv(os.path.join(path_out, 'regression',
                          'wsi_wd_test6', 'part3',
                          'multivar_separate_coefs.csv'))
