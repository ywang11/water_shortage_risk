"""
20190910
ywang254@utk.edu

Convert China province polygon to NetCDF mask at 0.5 x 0.5 degrees.
"""
from osgeo import gdal, ogr, osr
from shapely.geometry import shape
import regionmask
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
from utils.paths import path_data, path_out
import os
import cartopy.crs as ccrs
from utils.constants import province


f = gdal.OpenEx(os.path.join(path_data, 'China_Provinces', 
                             'China-provincial_level.shp'))

shp = f.GetLayer(0)

n_features = shp.GetFeatureCount()


#ch_to_en = {'安徽省': 'Anhui', 
#            '北京市': 'Beijing', 
#            '福建省': 'Fujian',
#            '甘肃省': 'Gansu', 
#            '广东省': 'Guangdong',
#            '广西壮族自治区': 'Guangxi', 
#            '贵州省': 'Guizhou',
#            '海南省': 'Hainan', 
#            '河北省': 'Hebei', 
#            '河南省': 'Henan', 
#            '黑龙江省': 'Heilongjiang', 
#            '湖北省': 'Hubei', 
#            '湖南省': 'Hunan', 
#            '吉林省': 'Jilin', 
#            '江苏省': 'Jiangsu', 
#            '江西省': 'Jiangxi', 
#            '辽宁省': 'Liaoning',
#            '内蒙古自治区': 'Inner Mongolia',
#            '宁夏回族自治区': 'Ningxia', 
#            '青海省': 'Qinghai', 
#            '山东省': 'Shandong',
#            '山西省': 'Shanxi',
#            '陕西省': 'Shaanxi', 
#            '上海市': 'Shanghai',
#            '四川省': 'Sichuan',
#            '天津市': 'Tianjin',
#            '西藏自治区': 'Tibet',
#            '新疆维吾尔自治区': 'Xinjiang',
#            '云南省': 'Yunnan',
#            '浙江省': 'Zhejiang',
#            '重庆市': 'Chongqing'}


# Convert the projection
source = shp.GetSpatialRef()
target = osr.SpatialReference()
target.ImportFromEPSG(4326)
transform = osr.CoordinateTransformation(source, target)


count = 0
numbers = []
names = []
abbrevs = []
geoms = []
for f_ind in range(n_features):
    feature = shp.GetFeature(f_ind)

    feature.geometry().Transform(transform)

    #print feature.GetField('')
    first = feature.ExportToJson(as_object = True) # (GeoJSON format)

    #prv = first['properties']['省级_1']
    prv = first['properties']['ID']
    # print(prv)

    if not prv in province:
        continue

    which = np.where([prv == pp for pp in province])[0][0]
    print(which, prv)

    if (type(prv) != type(None)) & (prv != 'Taiwan'):
        # Create shapely Polygon
        shp_geom = shape(first['geometry'])

        numbers.append(which)
        #names.append(ch_to_en[prv])
        #abbrevs.append(ch_to_en[prv])
        names.append(prv)
        abbrevs.append(prv)
        geoms.append(shp_geom)

        count += 1

CHN_poly = regionmask.Regions_cls('CHN_poly', numbers, names, abbrevs,
                                  geoms)

# (1) Create dataset at 0.5o level.
ds = xr.Dataset(coords={'lon': np.arange(-179.75, 179.76, 0.5),
                        'lat': np.arange(-89.75, 89.76, 0.5)})

# define the province regions
ds = CHN_poly.mask(ds).to_dataset()
ds.attrs['Key'] = '; '.join([str(i) + ': ' + names[i] \
                             for i in range(len(names))])

# store to file: 0 to 31
ds.to_netcdf(os.path.join(path_out, 'CHN_mask_0.5.nc'))

# make plot showing different regions
fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()})
ax.set_extent([20, 180, 0, 60])
ds.region.plot()
fig.savefig(os.path.join(path_out, 'CHN_mask_0.5.png'))


"""
# Too slow

# (2) Create dataset at 0.0833o level.
data = xr.open_dataset(os.path.join(path_data, 'WaterDemand', 'global', 
                                    'global_historical_PDomUse_month_millionm3_5min_1960_2014.nc4'))
ds = xr.Dataset(coords={'lon': data.longitude.copy(deep = True),
                        'lat': data.latitude.copy(deep = True)})
data.close()

# define the province regions
ds = CHN_poly.mask(ds).to_dataset()
ds.attrs['Key'] = [str(i) + ': ' + names[i] for i in range(31)]

# store to file: 0 to 31
ds.to_netcdf(os.path.join(path_out, 'CHN_mask_0.0833.nc'))

# make plot showing different regions
fig, ax = plt.subplots()
ds.region.plot()
fig.savefig(os.path.join(path_out, 'CHN_mask_0.0833.png'))
"""
