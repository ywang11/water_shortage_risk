"""
20200523
ywang254@utk.edu

Power spectrum of the climate indices.

Cut-off at half-cycle of the highest spectra:
  (1) AMO = 5
  (2) IDMI = 3
  (3) NAO = 2
  (4) PDO = 3
  (5) SOI = 4
"""
import os
import pandas as pd
import numpy as np
from utils.paths import path_data, path_out
from scipy.signal import spectrogram, welch
import matplotlib.pyplot as plt
import matplotlib as mpl


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5


inx = pd.read_csv(os.path.join(path_out, 'inx.csv'), index_col = 0)


fig, axes = plt.subplots(nrows = 5, ncols = 3, figsize = (6.5, 6.5))
fig.subplots_adjust(hspace = 0.01)
for cind, clim in enumerate(['AMO', 'IDMI', 'NAO', 'PDO', 'SOI']):
    #
    ax = axes[cind, 0]
    ax.plot(inx.index, inx.loc[:, clim], '-k')
    ax.set_ylabel(clim)
    if cind == 0:
        ax.set_title('Time Series')
    if cind == 4:
        ax.set_xlabel('Year')
    else:
        ax.set_xticklabels([])

    #
    #ax = axes[cind, 1]
    #freqs, times, spectra = spectrogram(inx.loc[:, clim])
    #ax.imshow(spectra, aspect = 'auto', cmap = 'hot_r', origin = 'lower')
    #ax.set_ylabel('Frequency band')
    #if cind == 0:
    #    ax.set_title('Spectrogram')
    #if cind == 4:
    #    ax.set_xlabel('Time window')

    #
    ax = axes[cind, 1]
    freqs, psd = welch(inx.loc[:, clim])
    period = 1 / freqs
    ax.plot(period, psd)
    #ax.set_ylabel('Power')
    if cind == 0:
        ax.set_title('PSD: power spectral density')
    if cind == 4:
        ax.set_xlabel('Period') # 'Frequency')
    else:
        ax.set_xticklabels([])

    #
    ax = axes[cind, 2]
    freqs, psd = welch(inx.loc[:, clim])
    period = 1 / freqs
    ax.plot(period, psd)
    ax.set_xlim([2, 12])
    ax.set_xticks(range(2, 13))
    #ax.set_ylabel('Power')
    if cind == 0:
        ax.set_title('PSD: power spectral density')
    if cind == 4:
        ax.set_xlabel('Period') # 'Frequency')
    else:
        ax.set_xticklabels([])
    ax.grid(True, which = 'major', axis = 'x')

fig.savefig(os.path.join(path_out, 'inx_spectra.png'), dpi = 600.,
            bbox_inches = 'tight')
plt.close()
