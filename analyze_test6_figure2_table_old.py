"""
2021/10/09
ywang254@utk.edu
"""
import os
import numpy as np
import pandas as pd
from utils.constants import *
from utils.paths import *


def read_data(var):
    goal = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                    'part2', 'goal_' + var + '_bc.csv'),
                       index_col = 0)

    restored = {}
    for nn in range(npcs + 1):
        restored[nn] = pd.read_csv(os.path.join(path_out, 'regression',
                                                'wsi_wd_test6',
                                                'part4',
                                                'restored_' + var + '_' + \
                                                str(nn) + '_bc.csv'),
                                   index_col = 0)

    le = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 
                                  'part3', 'multivar_le.csv'),
                     parse_dates = True, 
                     index_col = 0, header = [0,1]).loc[:, (slice(None), '0')]
    le.columns = le.columns.droplevel(1)
    le = le.groupby(le.index.year, axis = 0).mean().loc[1920:, :]
    le_signs = {}
    for nn in range(npcs + 1):
        le_signs[(nn, 'positive')] = le.index[ le[str(nn)] > 0. ]
        le_signs[(nn, 'negative')] = le.index[ le[str(nn)] < 0. ]
    return goal, restored, le_signs


npcs = 1
count = 0
for var in ['wsi', 'wd']:
    goal, restored, le_signs = read_data(var)

    if count == 0:
        summary = pd.DataFrame(np.nan, 
                               index = pd.MultiIndex.from_product([['WSI','WD'], 
                                                                   ['Original',
                                                                    'No SVD1','SVD1 +','SVD1 -',
                                                                    'No SVD2','SVD2 +','SVD2 -'],
                                                                   ['Mean','Stdev','C.V.']]),
                               columns = goal.columns)


    summary.loc[(var.upper(), 'Original', 'Mean'), :] = goal.mean(axis = 0)
    summary.loc[(var.upper(), 'Original', 'Stdev'), :] = goal.std(axis = 0)
    summary.loc[(var.upper(), 'Original', 'C.V.'), :] = goal.std(axis = 0)/goal.mean(axis = 0)

    no_variability = summary.columns[summary.loc[(var.upper(), 'Original', 'C.V.'), :] < 1e-6]
    summary.loc[(var.upper(), 'Original', 'Mean'), no_variability] = np.nan
    summary.loc[(var.upper(), 'Original', 'Stdev'), no_variability] = np.nan
    summary.loc[(var.upper(), 'Original', 'C.V.'), no_variability] = np.nan

    for nn in range(npcs + 1):
        summary.loc[(var.upper(), 'No SVD'+str(nn+1), 'Mean'), :] = restored[nn].mean(axis = 0)
        summary.loc[(var.upper(), 'No SVD'+str(nn+1), 'Stdev'), :] = restored[nn].std(axis = 0)
        summary.loc[(var.upper(), 'No SVD'+str(nn+1), 'C.V.'), :] = restored[nn].std(axis = 0) / \
                                                                    restored[nn].mean(axis = 0)

        no_variability = summary.columns[summary.loc[(var.upper(), 'No SVD'+str(nn+1), 'C.V.'),
                                                     :] < 1e-6]
        summary.loc[(var.upper(), 'No SVD'+str(nn+1), 'Mean'), no_variability] = np.nan
        summary.loc[(var.upper(), 'No SVD'+str(nn+1), 'Stdev'), no_variability] = np.nan
        summary.loc[(var.upper(), 'No SVD'+str(nn+1), 'C.V.'), no_variability] = np.nan


        for sign, sign_ in zip(['+','-'], ['positive','negative']):
            summary.loc[(var.upper(), 'SVD'+str(nn+1)+' '+sign, 'Mean'), :] = \
                goal.loc[le_signs[(nn,sign_)], :].mean(axis = 0)
            summary.loc[(var.upper(), 'SVD'+str(nn+1)+' '+sign, 'Stdev'), :] = \
                goal.loc[le_signs[(nn,sign_)], :].std(axis = 0)
            summary.loc[(var.upper(), 'SVD'+str(nn+1)+' '+sign, 'C.V.'), :] = \
                goal.loc[le_signs[(nn,sign_)], :].std(axis = 0) / \
                goal.loc[le_signs[(nn,sign_)], :].mean(axis = 0)

            no_variability = summary.columns[summary.loc[(var.upper(),
                                                          'SVD'+str(nn+1)+' '+sign,
                                                          'C.V.'), :] < 1e-6]
            summary.loc[(var.upper(), 'SVD'+str(nn+1)+' '+sign, 'Mean'), no_variability] = np.nan
            summary.loc[(var.upper(), 'SVD'+str(nn+1)+' '+sign, 'Stdev'), no_variability] = np.nan
            summary.loc[(var.upper(), 'SVD'+str(nn+1)+' '+sign, 'C.V.'), no_variability] = np.nan

    count += 1

summary_summary = pd.DataFrame({'Mean': summary.mean(axis = 1),
                                'Min': summary.min(axis = 1),
                                'Max': summary.max(axis = 1)})

summary = pd.concat([summary, summary_summary], axis = 1)

summary.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'analyze', 
                            'figure2_table.csv'))
