"""
2021/10/10
ywang254@utk.edu

Check the relative importance of the top five predictors for predictand.
"""
import pandas as pd
import numpy as np
import os
from utils.paths import path_data, path_out
from utils.constants import *
from utils.io import load_training_data, load_met
from time import time
import itertools as it
from joblib import load
from regression_both_test6_part2 import subset_predictors, min_max, max_max, mid_max
from utils.analysis import get_pr_subset


def get_importance(var, pr):
    # Selected hyperparameters of the Random Forest model, based on the results
    # of "regression_both_test6.py" and "regression_both_test6_part1_plot.py"
    # Restrict the number of predictors to >= 1
    params = {'wd': {'n_estim': 400, 'n_features': 15,
                     'n_split': max_max},
              'wsi': {'n_estim': 100, 'n_features': 5,
                      'n_split': max_max}}
    max_lag = 1
    
    pred_list = subset_predictors(var, pr, params[var])
        
    reg = load(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                            'part2', 'model', 'fit_' + var + '_' + \
                            pr + '.joblib'))

    ind = np.argsort(reg.feature_importances_)[::-1]
    pred_list = [ii[0] + ' (' + ii[1] + ',' + ii[2] + ')' for ii in pred_list[ind[:5]]]

    return pd.Series(pred_list,
                     index = ['1','2','3','4','5'])


importance = pd.DataFrame('                                      ',
                          index = pd.MultiIndex.from_product([['wsi','wd'], province]),
                          columns = ['1','2','3','4','5'])
for vind, var in enumerate(['wsi', 'wd']):
    province_subset = get_pr_subset(var)

    for pind, pr in enumerate(province_subset):
        imprts = get_importance(var, pr)
        importance.loc[(var,pr), :] = imprts
importance.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part2', 
                               'importance_summary.csv'))
