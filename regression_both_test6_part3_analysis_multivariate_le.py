"""
2021/3/13
ywang254@utk.edu

Plot the correlation between the left expansion coefficients and the oceanic 
 indices.
"""
import pandas as pd
import numpy as np
import os
from utils.io import load_met
from utils.paths import path_data, path_out
from utils.constants import *
from utils.analysis import detrend_2d, deseasonalize_2d
import xarray as xr
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import gridspec
import itertools as it
from warnings import filterwarnings
from regression_both_test6_part3_analysis_univariate \
    import get_met_data, get_sst, reshape_sst, restore_sst, match_time
from utils.plotting import ax_regress


#filterwarnings(action = 'error', category = FutureWarning)
#filterwarnings(action = 'ignore')


# setting: different lags does not have much impact
lag = 0
npcs = 5


if __name__ == '__main__':
    mpl.rcParams['font.size'] = 5
    mpl.rcParams['axes.titlesize'] = 5
    
    input_met0 = get_met_data(normalize = True, logpr = True)
    input_met, met_trend = detrend_2d(input_met0)
    input_met, met_seasonality = deseasonalize_2d(input_met)

    le  = pd.read_csv(os.path.join(path_out, 'regression',
                                   'wsi_wd_test6', 'part3',
                                   'multivar_le.csv'),
                      index_col = 0, header = [0, 1], parse_dates = True \
    ).loc[:, (slice(None), str(lag))]
    le.columns = le.columns.droplevel(1)
    inx = pd.read_csv(os.path.join(path_out, 'inx.csv'),
                      index_col = 0, header = [0, 1] \
    ).loc[:, (slice(None), 'Annual')]
    inx.columns = inx.columns.droplevel(1)

    #
    for season in [12, 3, 6, 9]:
        fig, axes = plt.subplots(npcs, len(xclim), 
                                 figsize = (6.5, 6.5))
        for ii in range(npcs):
            for jj, ix in enumerate(xclim):
                ax = axes[ii, jj]

                x = le.loc[le.index.month == season, str(ii)]
                x.index = x.index.year
                y = inx.loc[:, ix]
                y = y.loc[sorted(set(x.index) & set(y.index))]
                x = x.loc[y.index]

                ax_regress(ax, x.values, y.values, display = 'pearson',
                           args_pt = {'marker': 'o', 'lw': 0,
                                      'ms': 1})
    
                if ii == 0:
                    ax.set_title(ix)
                if jj == 0:
                    ax.set_ylabel('PC' + str(ii))
        fig.savefig(os.path.join(path_out, 'regression',
                                 'wsi_wd_test6', 'part3',
                                 'multivariate_le_' + str(season) + '.png'), 
                    dpi = 600.,
                    bbox_inches = 'tight')
        plt.close(fig)
