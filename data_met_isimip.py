"""
Convert the daily to monthly meteorological factors. Subset to China.
 Weight by population to province level.
CRU data.
"""
from utils.paths import *
from utils.constants import *
from utils.io import *
import xarray as xr
import pandas as pd
import numpy as np
import os
import itertools as it
from glob import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


# Read the population count in each grid cell.
hr = xr.open_dataset(os.path.join(path_out, 'pop_count_0.5.nc'))
pop = hr['pop'].values.copy()
hr.close()


# Read the China province mask.
h = xr.open_dataset(os.path.join(path_out, 'CHN_mask_0.5.nc'))
mask = h['region'].values.copy()
_, prov_to_num = parse_key(h.attrs['Key'])
h.close()


## Create the netcdf files.
#for m, v in it.product(model, met):
#    if v == 'pet':
#        f = sorted(glob(os.path.join(path_data, 'ISIMIP', v,
#                                     'pcrglobwb_' + m + \
#                                     '_hist_pressoc_potevap_monthly_*.nc')))
#        h = xr.open_mfdataset(f, decode_times = False)
#        htime = decode_time(h['time'])
#        #pd.date_range('1971-01-01', '2005-12-31', freq = 'MS')
#        val = h['potevap']
#        val['time'] = htime
#    else:
#        f = sorted(glob(os.path.join(path_data, 'ISIMIP', v,
#                                     v + '_bced_1960_1999_' +
#                                     m + '_historical_*.nc')))
#        h = xr.open_mfdataset(f)
#        htime = h['time'].to_index()
#        val = h[v]
#        val['time'] = htime
#
#    # Reverse latitude S->N
#    val = val[:, ::-1, :]
#    if v == 'pet':
#        val = val.load()
#    else:        
#        val = val.resample(time = '1M').mean().load()
#    h.close()
#
#    # Unit issues
#    if v == 'pr':
#        val = val * 86400
#        val.attrs['units'] = 'mm/d'
#    elif v == 'pet':
#        val = val * 86400
#        val.attrs['units'] = 'mm/d'
#    else:
#        val = val - 273.15
#        val.attrs['units'] = 'degC'
#
#    # Save
#    val.to_dataset(name = v).to_netcdf(os.path.join(path_out,
#                                                    'met_province',
#                                                    v + '_' + m + '.nc'))
#    # ---- plot
#    fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()})
#    ax.coastlines()
#    ax.set_extent([68.56, 140.01, 12.93, 55.8])
#    ax.contourf(val.lon, val.lat, val.mean(dim = ['time']))
#    fig.savefig(os.path.join(path_out, 'met_province',
#                             v + '_' + m + '.png'), dpi = 600)
#    plt.close(fig)


# Subset to province level & aggregate to seasonal
met_aggr = pd.DataFrame(np.nan,
                        index = range(1951, 2006),
                        columns = pd.MultiIndex.from_product([province, met,
                                                              model,
                        ['Annual', 'DJF', 'MAM', 'JJA', 'SON']]))
for m, v in it.product(model, met):
    hr = xr.open_dataset(os.path.join(path_out, 'met_province',
                                      v + '_' + m + '.nc'))
    val = hr[v].values.copy()
    hr.close()

    #
    for pr in province:
        prov_mask = np.abs(mask - prov_to_num[pr]) < 1e-6

        wgts = np.where(prov_mask, pop, np.nan)
        wgts = wgts / np.nansum(wgts)

        if v == 'pet':
            if m == 'hadgem2-es':
                temp = pd.Series(np.nansum(np.nansum(val * wgts, axis = 2),
                                           axis = 1),
                                 index = pd.date_range('1971-01-01',
                                                       '2004-12-31',
                                                       freq = 'MS'))
                start = 1971
                end = 2004
            else:
                temp = pd.Series(np.nansum(np.nansum(val * wgts, axis = 2),
                                           axis = 1),
                                 index = pd.date_range('1971-01-01',
                                                       '2005-12-31',
                                                       freq = 'MS'))
                start = 1971
                end = 2005
        else:
            if m == 'hadgem2-es':
                temp = pd.Series(np.nansum(np.nansum(val * wgts, axis = 2),
                                           axis = 1),
                                 index = pd.date_range('1951-01-01',
                                                       '2004-12-31',
                                                       freq = 'MS'))
                start = 1951
                end = 2004
            else:
                temp = pd.Series(np.nansum(np.nansum(val * wgts, axis = 2),
                                           axis = 1),
                                 index = pd.date_range('1951-01-01',
                                                       '2005-12-31',
                                                       freq = 'MS'))
                start = 1951
                end = 2005

        met_aggr.loc[start:, (pr,v,m,'Annual')] = \
            temp.groupby(temp.index.year).mean()

        #
        for ii,ss in enumerate(['DJF','MAM','JJA','SON']):
            temp2 = temp.loc[temp.index.to_period('Q-NOV').quarter \
                             == (ii+1)]
            met_aggr.loc[:, (pr,v,m,
                             ss)] = temp2.groupby(temp2.index.year).mean()
met_aggr.to_csv(os.path.join(path_out, 'met_province', 'met_isimip.csv'))
