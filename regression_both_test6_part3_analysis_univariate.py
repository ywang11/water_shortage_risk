"""
2021/1/28
ywang254@utk.edu

Try MCA between provincial-scale precipitation and spatial SST anomalies.
"""
import pandas as pd
import numpy as np
import os
from utils.io import load_met
from utils.paths import path_data, path_out
from utils.constants import *
from utils.plotting import plot_province
from utils.analysis import detrend, deseasonalize
import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from time import time
import itertools as it
from warnings import filterwarnings
from xMCA import xMCA
from cartopy.util import add_cyclic_point
from dateutil.relativedelta import relativedelta


#filterwarnings(action = 'error', category = FutureWarning)
filterwarnings(action = 'ignore')


def get_met_data(normalize = False, logpr = False):
    for pind, pr in enumerate(province):
        # (note: only need to read max_lag = 0 for time series prediction)
        met_data = load_met(path_out, 'cru', pr, 0)
        values = met_data.values.reshape(-1, len(met), 4)
        values = values.swapaxes(2,1).reshape(-1, len(met))
        
        met_data = pd.DataFrame(values,
                                index = pd.date_range('1919-12-01',
                                                      freq = '3MS',
                                              periods = values.shape[0]),
            columns = pd.MultiIndex.from_tuples([(mm, pr) for mm in met]))
        if pind == 0:
            met_data_collect = met_data
        else:
            met_data_collect = pd.concat([met_data, met_data_collect],
                                         axis = 1)
    met_data_collect = met_data_collect.sort_index(axis = 1)

    # remove pre-1920 data because of quality problems in provinces like
    # Qinghai and Gansu
    met_data_collect = met_data_collect.loc[met_data_collect.index.year \
                                            >= 1919, :]

    # convert precipitation by log-transformation
    if logpr:
        for pr in province:
            met_data_collect.loc[:, ('pr', pr)] = \
                np.log(met_data_collect.loc[:, ('pr', pr)] + 1)

    # remove the temporal average & divided by standard deviation
    if normalize:
        met_data_collect = (met_data_collect - \
                            met_data_collect.mean(axis = 0)) / \
                            met_data_collect.std(axis = 0)
    return met_data_collect


def get_sst():
    hr = xr.open_dataset(os.path.join(path_data, 'ERSST_v5',
                                      'sst.mnmean.nc'))
    sst = hr['sst'].copy(deep = True)
    sst = sst.resample(time = 'QS-DEC').mean()
    sst = sst[1:(-1), :, :] # skip the first and last Dec b.c. incomplete.
    hr.close()

    #sst1, lons1 = add_cyclic_point(sst.values, sst.lon)
    #sst = xr.DataArray(sst1.data, dims = sst.dims,
    #                   coords = {'time': sst['time'],
    #                             'lat': sst['lat'],
    #                             'lon': lons1})
    return sst


def reshape_sst(sst):
    temp = sst.values.reshape(sst.shape[0], -1)
    retain = np.where(~np.isnan(temp[0, :]))[0]
    return xr.DataArray(temp[:, retain],
                        dims = ['time', 'grid'],
                        coords = {'time': sst['time'],
                                  'grid': retain})


def restore_sst(sst2d, retain, coords):
    sst_restored = np.full([len(coords[coords.dims[0]]),
                            len(coords[coords.dims[1]]) * \
                            len(coords[coords.dims[2]])], np.nan)
    sst_restored[:, retain] = sst2d
    sst_restored = sst_restored.reshape(len(coords[coords.dims[0]]),
                                        len(coords[coords.dims[1]]),
                                        len(coords[coords.dims[2]]))
    sst_restored = xr.DataArray(sst_restored,
                                dims = coords.dims,
                                coords = coords)
    return sst_restored


def match_time(sst0, met, lag):
    met_index = met.index
    met = met.iloc[lag:, :] # SST precedes met with lag

    sst0['time'] = sst0['time'].to_index().shift(3, freq = 'MS')

    tvec = sst0['time'].to_index()    
    temp = (tvec >= met.index[0]) & (tvec <= met.index[-1])
    sst0 = sst0[temp, :, :]
    tvec = tvec[temp]
    met = met.loc[tvec, :]
    return sst0, met


def plot_pc_sst(lp, le):
    fig = plt.figure(figsize = [6.5, 6.5])
    axes = np.empty(2, dtype = object)
    axes[0] = fig.add_subplot(2, 1, 1,
            projection = ccrs.PlateCarree(central_longitude = 180.))
    axes[1] = fig.add_subplot(2, 1, 2)

    ax = axes[0]
    ax.coastlines()
    cf = ax.contourf(lp['lon'], lp['lat'], lp, cmap = 'RdBu_r',
                     transform = ccrs.PlateCarree())
    ax.set_title('Singular pattern')
    plt.colorbar(cf, ax = ax, orientation = 'horizontal', shrink = 0.7)

    ax = axes[1]
    ax.plot(le['time'], le.values)
    ax.set_title('Expansion Coefs.')

    return fig, axes


def plot_patterns(lh, rhet, rphet):
    """Left-side homogeneous patterns (projection of the Expansion coef. 
                                       unto selfsame variable).
       Right-side heterogeneous patterns (projection of the Expansion coef.
                                          unto the predicted variable)."""
    fig, axes = plt.subplots(2, 2, figsize = (6.5, 6.5),
        subplot_kw = {'projection': ccrs.PlateCarree()})
    ax = plt.subplot(2, 1, 1,
                     projection = ccrs.PlateCarree(central_longitude = 180.))
    ax.coastlines()
    cf = ax.contourf(lh['lon'], lh['lat'], lh, cmap = 'RdBu_r',
                     transform = ccrs.PlateCarree())
    ax.set_title('SST homogeneous pattern')
    plt.colorbar(cf, ax = ax, shrink = 0.7)

    ax = axes[1, 0]
    ax.coastlines()
    ax.set_extent([67.1,136.2,7.7,53.9])
    cf = plot_province(ax, rhet)
    ax.set_title('Heterogeneous pattern')
    plt.colorbar(cf, ax = ax, shrink = 0.7)

    ax = axes[1, 1]
    ax.coastlines()
    ax.set_extent([67.1,136.2,7.7,53.9])
    cf = plot_province(ax, rphet.where(rphet <= 0.05))
    ax.set_title('p-Value of pattern')
    plt.colorbar(cf, ax = ax, shrink = 0.7)
    return fig, axes


if __name__ == '__main__':
    sst0 = get_sst()
    input_met0 = get_met_data()
    
    
    varname = 'tas' # try the meteorological variables differently
    npcs = 3 # The number of PCs to use in MCA; total > 80%
             # The three PCs have similar patterns.
    max_lag = 3 # since four seasons, consider effect < 1 year
    
    
    # Save the heterogeneous correlations & p-values.
    frac_collection = pd.DataFrame(np.nan,
                                   index = range(npcs),
                                   columns = range(1, 1 + max_lag))
    le_collection = pd.DataFrame(np.nan,
                                 index = input_met0.index,
        columns = pd.MultiIndex.from_product([range(npcs),
                                              range(1, 1 + max_lag)],
                                             names = ['PC', 'Lag']))
    rhet_collection = pd.DataFrame(np.nan, index = province,
        columns = pd.MultiIndex.from_product([range(npcs),
                                              range(1, 1  +max_lag)],
                                             names = ['PC', 'Lag']))
    rhet_collection_p = pd.DataFrame(np.nan, index = province,
        columns = pd.MultiIndex.from_product([range(npcs),
                                              range(1, 1 + max_lag)],
                                             names = ['PC', 'Lag']))
    for lag in range(1, max_lag + 1):
        input_sst, input_met = match_time(sst0, input_met0, lag)
    
        input_sst = reshape_sst(input_sst)
        input_sst, sst_trend = detrend(input_sst)
        input_sst, sst_seasonality = deseasonalize(input_sst)
    
        input_met = xr.DataArray(input_met.loc[:, varname],
                                 dims = ['time', 'province'],
                                 coords = {'time': input_met.index,
                                           'province': range(len(province))})
        input_met, met_seasonality = deseasonalize(input_met)
    
        # MCA
        # https://github.com/Yefee/xMCA
        # https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2005GL022800
        # (Sect 2.2)
        sst_ts = xMCA(input_sst, input_met)
        sst_ts.solver()
        lp, _ = sst_ts.patterns(n = npcs)
        le, _ = sst_ts.expansionCoefs(n = npcs)
        frac = sst_ts.covFracs(n = npcs)
        print(frac)
        frac_collection.loc[:, lag] = frac.values
    
        lh, _ = sst_ts.homogeneousPatterns(n = npcs)
        _, rhet, _, rphet = sst_ts.heterogeneousPatterns(n = npcs, 
            statistical_test = True)
    
        lp2 = restore_sst(lp.values, lp['grid'].values,
                          coords = xr.DataArray(dims = ['n', 'lat', 'lon'],
                                                coords = {'n': lp['n'],
                                                          'lat': sst0['lat'],
                                                          'lon': sst0['lon']} \
                          ).coords)
        lh2 = restore_sst(lh.values, lh['grid'].values,
                          coords = xr.DataArray(dims = ['n', 'lat', 'lon'],
                                                coords = {'n': lh['n'],
                                                          'lat': sst0['lat'],
                                                          'lon': sst0['lon']} \
                          ).coords)
        
        for n in range(npcs):
            fig, axes = plot_pc_sst(lp2[n, :, :], le[n, :])
            fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                     'part3', varname + '_lag_' + str(lag) + \
                                     '_pc_sst_' + str(n) + '.png'), dpi = 600.)
            plt.close(fig)
            
            
            fig, axes = plot_patterns(lh2[n, :], rhet[n, :], rphet[n, :])
            fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                     'part3', varname + '_lag_' + str(lag) + \
                                     '_het_sst_' + str(n) + '.png'),
                        dpi = 600.)
            plt.close(fig)
    
        for n in range(npcs):
            le_collection.loc[input_met['time'].to_index(),
                              (n, lag)] = le[n, :].values
            rhet_collection.loc[:, (n, lag)] = rhet[n, :].values
            rhet_collection_p.loc[:, (n, lag)] = rphet[n, :].values
    
        xr.Dataset({'leftHomoPatterns': lh2,
                    'rightHeteroPatterns': rhet,
                    'rightPval': rphet}).to_netcdf(os.path.join(path_out,
                                                                'regression',
                                                                'wsi_wd_test6',
                                                                'part3',
                                                                varname + \
                                                                '_lag_' + \
                                                                str(lag) + \
                                                                '_fit.nc'))
    frac_collection.to_csv(os.path.join(path_out, 'regression',
                                        'wsi_wd_test6', 'part3',
                                        varname + '_frac.csv'))
    le_collection.to_csv(os.path.join(path_out, 'regression',
                                      'wsi_wd_test6', 'part3',
                                      varname + '_le.csv'))
    writer = pd.ExcelWriter(os.path.join(path_out, 'regression',
                                         'wsi_wd_test6', 'part3',
                                         varname + '_rhet.xlsx'))
    rhet_collection.to_excel(writer, sheet_name = 'rhet')
    rhet_collection_p.to_excel(writer, sheet_name = 'pvalue')
    writer.close()
