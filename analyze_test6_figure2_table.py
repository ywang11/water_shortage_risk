"""
2021/10/09
ywang254@utk.edu
"""
import os
import numpy as np
import pandas as pd
from utils.constants import *
from utils.paths import *


def read_data(var):
    goal = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part2', 'goal_' + var + '_bc.csv'), index_col = 0)

    restored = {}
    for nn in range(npcs + 1):
        restored[nn] = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part4', 'restored_' + var + '_' + str(nn) + '_bc.csv'), index_col = 0)

    le = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part3', 'multivar_le.csv'), parse_dates = True, index_col = 0, header = [0,1]).loc[:, (slice(None), '0')]
    le.columns = le.columns.droplevel(1)
    le = le.groupby(le.index.to_period('Q-NOV').year, axis = 0).mean().loc[1920:, :]
    le_signs = {}
    for nn in range(npcs + 1):
        le_signs[(nn, 'positive')] = le.index[ le[str(nn)] > 0. ]
        le_signs[(nn, 'negative')] = le.index[ le[str(nn)] < 0. ]
    return goal, restored, le_signs


npcs = 1
count = 0
for var in ['wsi', 'wd']:
    goal, restored, le_signs = read_data(var)

    if count == 0:
        # Record the average percentage difference 
        summary = pd.DataFrame(np.nan, 
                               index = pd.MultiIndex.from_product([['WSI','WD'], ['Original', 'SVD1 +', 'SVD1 -', 'SVD2 +', 'SVD2 -']]),
                               columns = goal.columns)

    summary.loc[(var.upper(), 'Original'), :] = goal.loc[2012, :]

    for nn in range(npcs + 1):
        for sign, sign_ in zip(['+','-'], ['positive','negative']):
            summary.loc[(var.upper(), 'SVD'+str(nn+1)+' '+sign), :] = (goal.loc[le_signs[(nn,sign_)], :].median(axis = 0) - goal.loc[2012, :]) / goal.mean(axis = 0) * 100
    count += 1

summary = summary.T.loc[['Beijing', 'Tianjin', 'Hebei', 'Shanxi', 'Inner Mongolia', 'Liaoning', 'Jilin', 'Heilongjiang', 'Shanghai', 'Jiangsu', 'Zhejiang', 'Anhui', 'Fujian', 'Jiangxi', 'Shandong', 'Henan', 'Hubei', 'Hunan', 'Guangdong', 'Guangxi', 'Hainan', 'Chongqing', 'Sichuan', 'Guizhou', 'Yunnan', 'Shaanxi', 'Gansu', 'Qinghai', 'Ningxia', 'Xinjiang'], :]
summary.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'analyze', 'figure2_table.csv'))
