"""
20200601

Plot the outcome of data_wd_interplate.ncl
"""
from utils.paths import *
from utils.constants import *
from utils.io import *
import xarray as xr
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from matplotlib.colors import PowerNorm


for fn in ['PIrrWN', 'PLivWN']:
    hr = xr.open_dataset(os.path.join(path_out, 'PIrrWN.nc'))
    val = hr['var'].copy(deep = True).mean(dim = 'time')
    hr.close()
    
    
    fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()})
    ax.coastlines()
    ax.set_extent([68.56, 140.01, 12.93, 55.8])
    cf = ax.contourf(val.lon, val.lat, val, cmap = 'Spectral', extend = 'both',
                     levels = np.arange(0., 9.1, 0.25),
                     norm = PowerNorm(gamma = 0.5, vmin = 0., vmax = 9.))
    plt.colorbar(cf, ax = ax)
    fig.savefig(os.path.join(path_out, fn + '.png'), dpi = 600,
                bbox_inches = 'tight')
    plt.close(fig)

