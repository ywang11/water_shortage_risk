"""
20200523
ywang254@utk.edu

Organize the climate indices with lags based on the cut-off identified
 in "data_clim_spectra.py"

Cut-off at half-cycle of the highest spectra:
  (1) AMO = 5
  (2) IDMI = 3
  (3) NAO = 2
  (4) PDO = 3
  (5) SOI = 4
"""
import pandas as pd
import numpy as np
import os
from utils.paths import path_data, path_out


def add_lag(mat, nyearlag):
    for i in range(1, nyearlag + 1):
        temp = np.roll(mat[:,[0]], i) # original data is monthly
        temp[:i, :] = np.nan
        mat = np.concatenate([mat, temp], axis = 1)
    return mat


nyearlag_amo = 5
nyearlag_idmi = 3
nyearlag_nao = 2
nyearlag_pdo = 3
nyearlag_soi = 4


names_lagged = ['AMO_' + str(i) for i in range(nyearlag_amo+1)] + \
    ['idmi_' + str(i) for i in range(nyearlag_idmi+1)] + \
    ['NAO_' + str(i) for i in range(nyearlag_nao+1)] + \
    ['PDO_' + str(i) for i in range(nyearlag_pdo+1)] + \
    ['SOI_' + str(i) for i in range(nyearlag_soi+1)]


inx = pd.read_csv(os.path.join(path_out, 'inx.csv'), index_col = 0)


###### AMO
amo = add_lag(inx[['AMO']].values, nyearlag_amo)


###### idmi
idmi = add_lag(inx[['IDMI']].values, nyearlag_idmi)


###### NAO
nao = add_lag(inx[['NAO']].values, nyearlag_nao)


###### PDO
pdo = add_lag(inx[['PDO']].values, nyearlag_pdo)


###### SOI
soi = add_lag(inx[['SOI']].values, nyearlag_soi)


######
inx = pd.DataFrame(np.concatenate([amo, idmi, nao, pdo, soi], axis = 1),
                   index = inx.index,
                   columns = names_lagged)
inx.to_csv(os.path.join(path_out, 'inx_lagged.csv'))
