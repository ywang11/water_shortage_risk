import os
import numpy as np
import pandas as pd
from utils.constants import *
from utils.paths import *
import matplotlib.pyplot as plt
import matplotlib as mpl
from utils.plotting import ax_histogram
import itertools as it
from collections import OrderedDict
from matplotlib import gridspec
from scipy.stats import kstest


def read_data(prefix, var):
    data = pd.read_csv(os.path.join(path_out, 'risk_results', '20210422',
                                    prefix + '_' + var + '_all.csv'),
                       index_col = 0)

    if prefix == 'goal':
        year_list = range(1920,2020)
    else:
        year_list = range(1921,2020)
    data.index = pd.MultiIndex.from_arrays([province, sectors])
    data.columns = year_list

    return data


def read_le():
    le = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                  'part3', 'multivar_le.csv'),
                     parse_dates = True,
                     index_col = 0, header = [0,1]).loc[:, (slice(None), '0')]
    le.columns = le.columns.droplevel(1)
    le = le.groupby(le.index.year, axis = 0).mean().loc[1920:, :]
    le_signs = {}
    for nn in range(npcs + 1):
        le_signs[(nn, 'positive')] = le.index[ le[str(nn)] > 0. ]
        le_signs[(nn, 'negative')] = le.index[ le[str(nn)] < 0. ]
    return le_signs


def get_rank(var):
    if var == 'lwsr':
        filename = 'Fig4 rank_waterUse.xlsx'        
    else:
        filename = 'Fig5 rank_throughFlow.xlsx'
    rank_prov = pd.read_excel(os.path.join(path_out, 'risk_results', filename),
                              sheet_name = 'sortByProvince', engine = 'openpyxl',
                              index_col = 0, header = 0).index.values.astype(str)
    rank_prov = np.array([xx for xx in rank_prov if xx != 'nan'])
    rank_sector = pd.read_excel(os.path.join(path_out, 'risk_results', filename),
                                sheet_name = 'sortBySector', engine = 'openpyxl',
                                index_col = 0, header = 0).index.values.astype(str)
    rank_sector = np.array([xx for xx in rank_sector if xx != 'nan'])
    return rank_prov, rank_sector


def sort_data(data, var):
    rank_prov, rank_sector = get_rank(var)

    subset = {}
    subset['median'] = data.median(axis = 1).unstack().loc[rank_prov, :].loc[:, rank_sector]
    ##subset['q75'] = pd.Series(np.percentile(data, 75, axis = 1),
    ##                          data.index).unstack().loc[rank_prov, :].loc[:, rank_sector]
    ##subset['q95'] = pd.Series(np.percentile(data, 95, axis = 1),
    ##                          data.index).unstack().loc[rank_prov, :].loc[:, rank_sector]
    subset['q90'] = pd.Series(np.percentile(data, 90, axis = 1),
                              data.index).unstack().loc[rank_prov, :].loc[:, rank_sector]
    return subset


def ks_test(gl, rt):
    rank_prov, rank_sector = get_rank(var)

    pval = pd.Series(False, gl.index)
    for ii in range(gl.shape[0]):
        _, pval.iloc[ii] = kstest(gl.iloc[ii,:].values, rt.iloc[ii,:].values)
    return pval.unstack().loc[rank_prov, :].loc[:, rank_sector]


#
mpl.rcParams['font.size'] = 5.
mpl.rcParams['axes.titlesize'] = 5.
mpl.rcParams['axes.titleweight'] = 'bold'
clist = ['#9ecae1', '#fee0d2', '#e5f5e0']
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']


mrio = pd.read_excel(os.path.join(path_data, 
                                  'China-multi-regional-input-output-MRIO-table-2012.xlsx'),
                     sheet_name = 'Data', skiprows = 3, skipfooter = 8,
                     header = [0,1,2], engine = 'openpyxl').iloc[:, 1:]
province = list(mrio.iloc[:-6, 0])
prev_pr = province[0]
for ind, curr_pr in enumerate(province.copy()):
    if not isinstance(curr_pr, str):
        province[ind] = prev_pr
    else:
        prev_pr = curr_pr
sectors = list(mrio.iloc[:-6, 1])


npcs = 2
#ntop = 5 # plot the top 5
for var in ['lwsr', 'vwsr']:
    if var == 'lwsr':
        vmin = -20.
        vmax = 20.
    else:
        vmin = -10.
        vmax = 10.

    goal0 = read_data('goal', var)
    restored0 = {}
    for nn in range(npcs):
        restored0[nn] = read_data('restored_' + str(nn), var)
    le_signs = read_le()        

    goal = sort_data(goal0, var)
    restored = {}
    positive = {}
    negative = {}
    for nn in range(npcs):
        restored[nn] = sort_data(restored0[nn], var)
        positive[nn] = sort_data(goal0.loc[:, le_signs[(nn,'positive')]], var)
        negative[nn] = sort_data(goal0.loc[:, le_signs[(nn,'negative')]], var)

    pval_diff = {}
    for nn in range(npcs):
        pval_diff[nn] = ks_test(goal0, restored0[nn])

    pval_sign = {}
    for nn in range(npcs):
        pval_sign[nn] = ks_test(goal0.loc[:, le_signs[(nn,'positive')]],
                                goal0.loc[:, le_signs[(nn,'negative')]])

    #
    fig, axes = plt.subplots(4, 2, figsize = (6.3, 8.), sharex = True, sharey = True)
    fig.subplots_adjust(hspace = 0.07, wspace = 0.05)
    for ii,name in enumerate(['median', 'q90']):
        for jj,nn in it.product(range(2), range(npcs)):
            ax = axes[jj*npcs + ii,nn]

            if jj == 0:
                temp = (restored[nn][name] - goal[name]) / goal[name] * 100.
                ptemp = pval_diff[nn]
            else:
                temp = (positive[nn][name] - negative[nn][name]) / \
                       (positive[nn][name] + negative[nn][name]) * 200.
                ptemp = pval_sign[nn]

            cf = ax.imshow(np.ma.array(temp, mask = np.isnan(temp)),
                           vmin = vmin, vmax = vmax, cmap = 'rainbow', aspect = 0.85)

            for pp,qq in it.product(range(ptemp.shape[0]), range(ptemp.shape[1])):
                if ptemp.values[pp,qq] <= 0.05:
                    ax.text(qq-0.05, pp + 0.5, '*')

            if (jj == 0) & (ii == 0):
                ax.set_title('SVD ' + str(nn + 1))
            ax.set_xticks(range(len(temp.columns)))
            ax.set_xticklabels([])
            ax.set_yticks(range(len(temp.index)))
            ax.set_yticklabels([])
            if nn == 0:
                if jj == 0:
                    ax.set_ylabel('Realistic \u2212 counterfactual ' + name)
                else:
                    ax.set_ylabel('Positive \u2212 negative ' + name)
            ax.tick_params('both', length = 1.)
            ax.text(0.025, 1.02, lab[jj*2*npcs + ii*2 + nn], fontdict = {'weight': 'bold'},
                    transform = ax.transAxes)
    big = fig.add_axes([0.1, 0.1, 0.8, 0.75])
    big.patch.set_visible(False)
    big.spines['right'].set_visible(False)
    big.spines['top'].set_visible(False)
    big.set_xlim([0, len(temp.columns)])

    sector_list = []
    for sec in temp.columns:
        pos_blank = []
        count = 0
        for aa in range(sec.count(' ')):
            pos_blank.append(sec.find(' ', count))
            count += pos_blank[-1]
        too_long = np.where(np.array(pos_blank) > 20)[0]
        if len(too_long) > 0:
            sector_list.append( sec[:pos_blank[too_long[0]]] + '\n' + \
                                sec[(pos_blank[too_long[0]] + 1):] )
        else:
            sector_list.append(sec)

    big.set_xticks(np.arange(0.5, len(temp.columns)))
    big.set_xticklabels(sector_list, rotation = 90)
    big.set_ylim([0, len(temp.index)])
    big.set_yticks(np.arange(0.5, len(temp.index)))
    big.set_yticklabels(temp.index)

    cax = fig.add_axes([0.92, 0.125, 0.01, 0.75])
    plt.colorbar(cf, cax = cax)
    fig.savefig(os.path.join(path_out, 'risk_results', var + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
