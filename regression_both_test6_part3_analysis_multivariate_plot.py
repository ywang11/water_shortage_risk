""" Plot the left time series, left SVD pattern, and the right heterogeneous pattern.
    Display also the explained variance by PC0 and PC1, and the significances."""
import os
import numpy as np
import pandas as pd
import xarray as xr
from utils.paths import path_data, path_out
from utils.constants import *
from utils.plotting import plot_province
import xarray as xr
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
from mpl_toolkits import axisartist
from matplotlib import gridspec
import cartopy.crs as ccrs
import pingouin as pg
from datetime import datetime, timedelta


def read_left_coefs():
    le = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part3',
                                  'multivar_le.csv'),
                     index_col = 0, parse_dates = True,
                     header = [0,1]).loc[:, (slice(None), str(lag))]
    le.columns = le.columns.droplevel(1)
    return le


def read_lh_rhet():
    """ Left SVD pattern and right heterongeous pattern. """
    hr = xr.open_dataset(os.path.join(path_out, 'regression',
                                      'wsi_wd_test6', 'part3',
                                      'multivar_lag_' + str(lag) + '_fit.nc'))
    lh = hr['leftHomoPatterns'].copy(deep = True)
    rhet = hr['rightHeteroPatterns'].copy(deep = True)
    rphet = hr['rightPval'].copy(deep = True)
    hr.close()
    return lh, rhet, rphet

    
def read_inx():
    inx = pd.read_csv(os.path.join(path_out, 'inx.csv'), index_col = 0, header = [0,1])
    inx2 = pd.DataFrame(index = [], columns = inx.columns.levels[0])
    for season in ['DJF', 'MAM', 'JJA', 'SON']:
        temp = inx.loc[:, (slice(None), season)]
        temp.columns = temp.columns.droplevel(1)
        if season == 'DJF':
            date = '-12-01'
            temp.index = [datetime.strptime(str(yy-1) + date, '%Y-%m-%d') \
                          for yy in inx.index]
        else:
            if season == 'MAM':
                date = '-03-01'
            elif season == 'JJA':
                date = '-06-01'
            else:
                date = '-09-01'
            temp.index = [datetime.strptime(str(yy) + date, '%Y-%m-%d') \
                          for yy in inx.index]
        inx2 = pd.concat([inx2, temp], axis = 0)
    inx2 = inx2.sort_index()
    return inx2


# MODIFY
npcs = 1 # The first two PCs were used in analyzing the meteorological variables
lag = 0


# 
le = read_left_coefs()
lh, rhet, rphet = read_lh_rhet()
inx = read_inx()


#
mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
clist = ['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00']
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']


fig = plt.figure(figsize = (6.5, 8), constrained_layout = True)
gs = gridspec.GridSpec(2, 1, hspace = 0.1, wspace = 0.18,
                       height_ratios = [1, 2.2])
gs1 = gridspec.GridSpecFromSubplotSpec(2, 3, subplot_spec = gs[0],
                                       hspace = 0.17, wspace = 0.35,
                                       width_ratios = [1, 1, 1.3])
gs2 = gridspec.GridSpecFromSubplotSpec(4, 3, subplot_spec = gs[1],
                                       hspace = 0.15, wspace = 0.05)
axes = {'ts': np.empty(2, dtype = object),
        'map':np.empty(2, dtype = object),
        'prov': np.empty((4,3), dtype = object)}
for ii in range(2):
    axes['ts'][ii] = host_subplot(gs1[ii,:2], axes_class = axisartist.Axes)
    axes['map'][ii] = fig.add_subplot(gs1[ii,2],
                                      projection = ccrs.PlateCarree(central_longitude = 180.))
for ii in range(len(met)):
    axes['prov'][ii//3,np.mod(ii,3)] = fig.add_subplot(gs2[ii//3, np.mod(ii,3)],
                                                       projection = ccrs.PlateCarree())
    axes['prov'][2 + (ii//3),np.mod(ii,3)] = fig.add_subplot(gs2[2+(ii//3), np.mod(ii,3)],
                                                             projection = ccrs.PlateCarree())

#
for ii in range(2):
    #
    ax = axes['ts'][ii]
    ax.plot(le.index, le[str(ii)], '-', color = '#1f78b4', lw = 0.5)
    ax.set_xlim([le.index[0], le.index[-1]])
    ax.set_ylabel('le SVD-' + str(ii+1))
    if ii == 0:
        ax.set_xticklabels([])
    ax.text(0.025, 1.05, lab[ii], transform = ax.transAxes,
            fontdict = {'weight': 'bold'})
    ax.tick_params('both', length = 1.)
    ax.axis['bottom'].set_visible(False)
    ax.axis['top'].set_visible(False)
    ax.get_xaxis().set_visible(True)
    ax.spines['bottom'].set_visible(True)
    ax.spines['top'].set_visible(True)
    ax.get_xaxis().set_tick_params(direction='out')
    ax.tick_params(top = "off")
    ax.axis['left'].major_ticks.set_tick_out(True)
    ax.axis['left'].major_ticks.set_ticksize(1)

    corr = pd.DataFrame(np.nan, index = xclim, columns = ['corr', 'pval'])
    for skip in xclim:
        stats = pg.partial_corr(pd.concat([le[[str(ii)]], inx], axis = 1),
                                x = skip, y = str(ii),
                                covar = [xx for xx in xclim if xx != skip])
        corr.loc[skip, 'corr'] = stats['r'].values[0]
        corr.loc[skip, 'pval'] = stats['p-val'].values[0]
    corr.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part3',
                             'multivar_le_corr_PC' + str(ii) + '.csv'))
#    which_max = np.argmax(np.abs(corr['corr'].values))

    ax2 = ax.twinx()
    ax2.axis['right'].toggle(all=True)
    ax2.axis['right'].major_ticklabels.set_color('#f03b20')
    ax2.axis['right'].major_ticks.set_tick_out(True)
    ax2.axis['right'].major_ticks.set_ticksize(1)
    ax2.axis['left'].major_ticks.set_tick_out(True)
    ax2.axis['left'].major_ticks.set_ticksize(1)
    if ii == 0:
        use = 'PDO'
        ax2.plot(inx.index, - inx[use], '-', color = '#f03b20', lw = 0.5)
        ax2.text(0.06, 1.05, r'(-1 $\times$) ' + use + ' ' + r'$\rho$ = ' + \
                 ('%.3f' % corr.loc[use, 'corr'])+ \
                 ('*' if corr.loc[use, 'pval'] <= 0.05 else ''),
                 transform = ax.transAxes, color = '#f03b20')
        ax.set_xticklabels([])
    else:
        use = 'SOI'
        color = '#f03b20'
        ax2.plot(inx.index, inx[use], '-', color = color, lw = 0.5)
        ax2.text(0.06, 1.05, use + ' ' + r'$\rho$ = ' + \
                 ('%.3f' % corr.loc[use, 'corr'])+ \
                 ('*' if corr.loc[use, 'pval'] <= 0.05 else ''),
                 transform = ax.transAxes, color = color)

        use = 'AMO'
        color = '#31a354'
        ax3 = ax.twinx()
        ax3.axis['right'].toggle(all=True)
        ax3.axis['right'] = ax3.new_fixed_axis(loc="right", offset=(16, 0))
        ax3.axis['right'].major_ticklabels.set_color(color)
        ax3.axis['right'].major_ticks.set_tick_out(True)
        ax3.axis['right'].major_ticks.set_ticksize(1)
        ax3.axis['left'].major_ticks.set_tick_out(True)
        ax3.axis['left'].major_ticks.set_ticksize(1)
        ax3.plot(inx.index, inx[use], '-', color = color, lw = 0.5)
        ax3.text(0.33, 1.05, use + ' ' + r'$\rho$ = ' + \
                 ('%.3f' % corr.loc[use, 'corr'])+ \
                 ('*' if corr.loc[use, 'pval'] <= 0.05 else ''),
                 transform = ax.transAxes, color = color)
        ax3.set_xlim([le.index[0], le.index[-1]])
    ax2.set_xlim([le.index[0], le.index[-1]])
    ax2.tick_params('x', length = 1.)

    #
    ax = axes['map'][ii]
    ax.coastlines(lw = 0.5)
    cf1 = ax.contourf(lh['lon'], lh['lat'], lh[ii, :, :],
                      cmap = 'rainbow', levels = np.linspace(-0.6, 0.6, 31),
                      transform = ccrs.PlateCarree(), extend = 'both')
    ax.set_title('lh SVD-' + str(ii+1), pad = 2)
    ax.text(0.025, 1.05, lab[ii+2], transform = ax.transAxes,
            fontdict = {'weight': 'bold'})

    #
    for jj, var in enumerate(met):
        ax = axes['prov'][jj//3 + 2*ii, np.mod(jj,3)]
        ax.coastlines(lw = 0.5)
        ax.set_extent([69.6,137.3,17.1,54.5])
        cf2 = plot_province(ax,
                            rhet.sel(n = ii, var = var).where(rphet.sel(n = ii,
                                                                        var = var) <= 0.05),
                            levels = np.linspace(-0.6, 0.6, 31),
                            cmap = 'rainbow', extend = 'both')
        ax.set_title('rhet ' + var + ' SVD-' + str(ii+1), pad = 2)
        ax.text(0.05, 0.9, lab[4 + jj + len(met)*ii], transform = ax.transAxes,
                fontdict = {'weight': 'bold'})

cax1 = fig.add_axes([0.91, 0.1, 0.01, 0.8])
plt.colorbar(cf1, cax = cax1, orientation = 'vertical', ticks = np.arange(-0.6, 0.61, 0.1))

#
fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part3',
                         'multivar_summary.png'), dpi = 600., bbox_inches = 'tight')
plt.close(fig)
