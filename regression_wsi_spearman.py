"""
Correlation between the provincial average WSI and
(1) the precipitation at each grid within the province; Display on China map.
(2) the individual climate indices; Display on x-y panels.
"""
from utils.paths import *
from utils.io import *
from utils.constants import *
from utils.plotting import *
import xarray as xr
import pandas as pd
import numpy as np
import os
import itertools as it
from scipy.stats import spearmanr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature


# Read the China province mask.
h = xr.open_dataset(os.path.join(path_out, 'CHN_mask_0.5.nc'))
mask = h['region'].values.copy()
_, prov_to_num = parse_key(h.attrs['Key'])
h.close()


max_lag = 11


#
for v, m in it.product(met, model):
    # Read the provincial wsi.
    wsi = pd.read_csv(os.path.join(path_out, 'prv_wsi_' + m + '.csv'),
                      index_col = 0, parse_dates = True)

    # Read the meteorological data.
    h = xr.open_dataset(os.path.join(path_out, 'met_province',
                                     v + '_' + m + '.nc'),
                        decode_times = False)
    h['time'] = decode_time(h['time'])
    val = h[v].copy(deep = True).resample(time = '1Y').mean()
    vtime = val['time'].to_index()
    h.close()

    # Subset the provincial wsi and meteorological data
    wsi = wsi.loc[np.array([x in vtime.year for x in wsi.index.year]), :]
    val = val.loc[np.array([x in wsi.index.year for x in vtime.year]), :]

    # Correlation between the meteorological data and WSI.
    # ---- up to max_lag lags.
    corr = xr.DataArray(np.full([val.shape[1], val.shape[2], max_lag+1],
                                np.nan),
                        coords = {'lat': val['lat'],
                                  'lon': val['lon'],
                                  'lag': range(max_lag+1)},
                        dims = ['lat', 'lon', 'lag'])
    corr_p = xr.DataArray(np.full([val.shape[1], val.shape[2], max_lag+1],
                                  np.nan),
                          coords = {'lat': val['lat'],
                                    'lon': val['lon'],
                                    'lag': range(max_lag+1)},
                          dims = ['lat', 'lon', 'lag'])
    for lag, pr in it.product(range(max_lag+1), province):
        prov_mask = np.abs(mask - prov_to_num[pr]) < 1e-6

        temp = np.where(prov_mask, val.values, np.nan)
        for a, b in it.product(range(temp.shape[1]),
                               range(temp.shape[2])):
            if np.isnan(temp[0, a, b]):
                continue
            if lag == 0:
                x = temp[:, a, b]
            else:
                x = temp[:(-lag), a, b]
            y = wsi.loc[:, pr].values[lag:]
            result = spearmanr(x, y)
            corr[a, b, lag] = result.correlation
            corr_p[a, b, lag] = result.pvalue

    xr.Dataset({'corr': corr, 'corr_p': corr_p}).to_netcdf( \
        os.path.join(path_out, 'regression', 'wsi_spearman',
                     'corr_' + v + '_' + m + '.nc'))



    # Plot
    fig, axes = plt.subplots(nrows = 4, ncols = 3, figsize = (18, 12),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for lag in range(max_lag+1):
        ax = axes.flat[lag]
        ax.coastlines()
        ax.set_extent([68.56, 140.01, 12.93, 55.8])

        chn_file = os.path.join(path_data, 'China_Provinces',
                                'China-provincial_level.shp')
        shape_feature = ShapelyFeature(Reader(chn_file).geometries(), 
                                       ccrs.PlateCarree(), edgecolor = 'black', 
                                       facecolor = [0, 0, 0, 0])
        ax.add_feature(shape_feature)
    
        cf = ax.contourf(corr.lon.values, corr.lat.values, corr[:,:,lag], 
                         cmap = 'rainbow', levels = np.arange(-1, 1.05, 0.1))
        stipple(ax, val.lat, val.lon, corr_p[:,:,lag], thresh = 0.05)
        ax.set_title('lag = ' + str(lag))

    cax = fig.add_axes([0.1, 0.05, 0.8, 0.01])
    plt.colorbar(cf, cax = cax, orientation = 'horizontal')

    fig.savefig(os.path.join(path_out, 'regression', 'wsi_spearman',
                             'corr_' + v + '_' + m + '.png'), dpi = 600.)
    plt.close(fig)
