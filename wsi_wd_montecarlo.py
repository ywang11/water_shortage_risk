"""
20200222

Simulate WSI and WIP_{ag} (convert to WI_{ag}) using the contrast climate
indice conditions of 2012, historical averages (1961-1990), and Monte Carlo
samples of residual distributions.

Bias-correct the results using the difference between 2012 observed and
simulated WSI and WIP_{ag} (convert to WI_{ag}).
"""
import pandas as pd
import numpy as np
import os


def convert_p2e():
    """
    Convert agricultural water use from per km2 to per yuan.
    """
    



# Observed WSI



# Observed WIP_{ag}
