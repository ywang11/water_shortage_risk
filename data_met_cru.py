"""
Convert the daily to monthly meteorological factors. Subset to China. 
 Weight by population to province level.
CRU data.
"""
from utils.paths import *
from utils.constants import *
from utils.io import *
import xarray as xr
import pandas as pd
import numpy as np
import os
import itertools as it
from glob import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


# Read the population count in each grid cell.
hr = xr.open_dataset(os.path.join(path_out, 'pop_count_0.5.nc'))
pop = hr['pop'].values.copy()
hr.close()


# Read the China province mask.
h = xr.open_dataset(os.path.join(path_out, 'CHN_mask_0.5.nc'))
mask = h['region'].values.copy()
_, prov_to_num = parse_key(h.attrs['Key'])
h.close()


# Create the netcdf files.
for v in met:
    if v == 'pet':
        v2 = 'pet'
    elif v == 'pr':
        v2 = 'pre'
    elif v == 'tasmin':
        v2 = 'tmn'
    elif v == 'tasmax':
        v2 = 'tmx'
    else:
        v2 = 'tmp'

    h = xr.open_dataset(os.path.join(path_data, 'CRU',
                                     'cru_ts4.04.1901.2019.' + v2 + '.dat.nc'))
    val = h[v2].copy(deep = True).load()
    htime = h['time'].to_index()
    h.close()

    # Unit issues
    if v == 'pr':
        val = val / htime.daysinmonth.values.reshape(-1, 1, 1)
        val.attrs['units'] = 'mm/d'
    elif v == 'pet':
        val.attrs['units'] = 'mm/d'
    else:
        val.attrs['units'] = 'degC'

    # Save
    val.to_dataset(name = v).to_netcdf(os.path.join(path_out,
                                                    'met_province',
                                                    v + '_cru.nc'))
    # ---- plot
    fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()})
    ax.coastlines()
    ax.set_extent([68.56, 140.01, 12.93, 55.8])
    cf = ax.contourf(val.lon, val.lat, val.mean(dim = ['time']))
    plt.colorbar(cf)
    fig.savefig(os.path.join(path_out, 'met_province',
                             v + '_cru.png'), dpi = 600)
    plt.close(fig)


# Subset to province level & aggregate to seasonal
met_aggr = pd.DataFrame(np.nan,
                        index = range(1901, 2020),
                        columns = pd.MultiIndex.from_product([province, met,
                        ['Annual', 'DJF', 'MAM', 'JJA', 'SON']]))
for v in met:
    hr = xr.open_dataset(os.path.join(path_out, 'met_province',
                                      v + '_cru.nc'))
    val = hr[v].values.copy()
    hr.close()

    #
    for pr in province:
        prov_mask = np.abs(mask - prov_to_num[pr]) < 1e-6

        wgts = np.where(prov_mask, pop, np.nan)
        wgts = wgts / np.nansum(wgts)

        temp = pd.Series(np.nansum(np.nansum(val * wgts, axis = 2),
                                   axis = 1),
                         index = pd.date_range('1901-01-01',
                                               '2019-12-31', freq = 'MS'))

        #
        met_aggr.loc[:, (pr,v,'Annual')] = \
            temp.groupby(temp.index.year).mean()

        #
        for ii,ss in enumerate(['DJF','MAM','JJA','SON']):
            temp2 = temp.loc[temp.index.to_period('Q-NOV').quarter \
                             == (ii+1)]
            met_aggr.loc[:, (pr,v,
                             ss)] = temp2.groupby(temp2.index.year).mean()
met_aggr.to_csv(os.path.join(path_out, 'met_province', 'met_cru.csv'))
