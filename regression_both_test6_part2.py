"""
20200803
ywang254@utk.edu

Use sklearn's built-in recursive feature elimitation and cross-validated
 selection of the best number of features to find the regression relationship
 between predictors and each province's
(1) Water Stress Index (the ratio of water withdrawal to total available 
                        surface water)
(2) Water Intensity of Crops in Physical Units (m3 per year per km2)
Predict for the historical range of precipitation values.
"""
import pandas as pd
import numpy as np
import os
from utils.paths import path_data, path_out
from utils.constants import *
from utils.io import load_training_data, load_met
import matplotlib.pyplot as plt
from scipy.stats import pearsonr
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.feature_selection import RFECV
from sklearn.metrics import r2_score, make_scorer
from itertools import combinations, product
from time import time
import multiprocessing as mp
import itertools as it
from joblib import dump


def min_max(x):
    return int(np.sqrt(x))
def mid_max(x):
    return int(0.5 * (np.sqrt(x) + x))
def max_max(x):
    return int(x)


def subset_predictors(var, pr, params_):
    data = pd.read_csv(os.path.join(path_out, 'regression',
                                    'wsi_wd_test6', 'part1',
                                    var + '_norm_importance_' + pr + '.csv'),
                       index_col = [0,1,2], header = [0,1,2])
    temp = data.loc[(params_['n_features'],
                     params_['n_split'].__name__,
                     params_['n_estim']), :]
    pred_list = temp.index[~np.isnan(temp)]
    return pred_list


# Selected hyperparameters of the Random Forest model, based on the results
# of "regression_both_test6.py" and "regression_both_test6_part1_plot.py"
# Restrict the number of predictors to >= 1
params = {'wd': {'n_estim': 400, 'n_features': 15,
                 'n_split': max_max},
          'wsi': {'n_estim': 100, 'n_features': 5,
                  'n_split': max_max}}


# Percent to reserve for validation
pct = 0.3
# Random seed
rf_seed = 999 # random forest regressor
tv_seed = 888 # train/validation split
# maximum number of lags
max_lag = 2


if __name__ == '__main__':
    # The R2, Pearson correlation, and p-value of the prediction by the
    # selected predictors on the validation set.
    performance = pd.DataFrame(data = np.nan,
                               index = province,
                               columns = pd.MultiIndex.from_product( \
                [['wsi', 'wd'], ['R2', 'Corr', 'pVal']]))
    
    #
    for var in ['wsi', 'wd']: 
        for pind, pr in enumerate(province):
            pr = province[pind]
    
            goal, met_data = load_training_data(path_out, var, pr, max_lag)
    
            # subset the met data to the number of selected features
            pred_list = subset_predictors(var, pr, params[var])
            met_data = met_data.loc[:, pred_list]
    
            # construct the cross-validation set
            X = met_data.values
            y = goal.values
    
            temp = np.isfinite(y) & np.isfinite(np.sum(X, axis = 1))
            X = X[temp, :]
            y = y[temp]
    
            # fit model and extract performance
            X_train, X_test, y_train, y_test = train_test_split( \
                X, y, test_size = pct, random_state = tv_seed)
    
            reg = RandomForestRegressor(n_estimators = params[var]['n_estim'],
                                        max_features = params[var]['n_split' \
                                        ](params[var]['n_features']),
                                        random_state = rf_seed,
                                        oob_score = True)
            reg = reg.fit(X_train, y_train)

            dump(reg, os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                   'part2', 'model', 'fit_' + var + '_' + \
                                   pr + '.joblib'))

            pred = reg.predict(X_test)
            r, p_value = pearsonr(pred, y_test)

            performance.loc[pr, (var,'R2')] = reg.score(X_test, y_test)
            performance.loc[pr, (var,'Corr')] = r
            performance.loc[pr, (var,'pVal')] = p_value
    
            # predict using new met data
            if pind == 0:
                goal2 = pd.DataFrame(np.nan, index = range(1920, 2020),
                                     columns = province)
            if (p_value <= 0.05) & (performance.loc[pr, (var,'R2')] > 0):
                met_data2 = load_met(path_out, 'cru', pr, max_lag)
                met_data2 = met_data2.loc[:, pred_list]
                retain = np.isfinite(np.sum(met_data2.values, axis = 1))
                y_pred = reg.predict(met_data2.values[retain, :])

                goal2.loc[retain, pr] = y_pred
            else:
                goal2.loc[:, pr] = np.nan
    
        goal2.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                  'part2', 'goal_' + var + '.csv'))
    performance.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                    'part2', 'performance.csv'))
