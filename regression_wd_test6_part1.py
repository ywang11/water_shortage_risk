"""
20200202
ywang254@utk.edu

Rank the importance of predictors in the regression relationship between 
meteorological index and each province's
(1) Water Stress Index (the ratio of water withdrawal to total available 
                        surface water)
(2) Water Intensity of Crops in Physical Units (m3 per year per km2)

Test the best 1, 2, 3, 4, 5, 6, 7 ... predictors.
"""
import pandas as pd
import numpy as np
import os
from utils.io import load_training_data
from utils.paths import path_data, path_out
from utils.constants import *
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split, cross_val_score
from itertools import combinations, product
from time import time
import multiprocessing as mp
import itertools as it


######################################################
# Hyperparameters to test for the Random Forest model.
######################################################
# Number of trees
n_estim_list = [100, 200, 400, 800, 1600]
# Number of input variables at each split
def min_max(x):
    return int(np.sqrt(x))
def mid_max(x):
    return int(0.5 * (np.sqrt(x) + x))
def max_max(x):
    return int(x)
n_split_list = [min_max, mid_max, max_max]
# Number of features to retain in the model
n_features_list = list(range(5, 41, 5))
# Percent to reserve for validation
pct = 0.3
# Number of folds in the cross-validation part
nfold = 5
# Random seed
rf_seed = 999
tv_seed = 888
# maximum number of lags
max_lag = 1


#
pred_list = list(it.product(met, season_list2,
                            ['Lag ' + str(lag) for \
                             lag in range(max_lag + 1)]))


#
##for prv in province:
def calc(prv):
    # Cross-validation & validation test performance of the pruned model
    r2 = pd.DataFrame(data = np.nan,
                      index = pd.MultiIndex.from_product( \
        [n_features_list, [i.__name__ for i in n_split_list],
         n_estim_list],
                      names = ['n_features', 'n_split', 'n_estim']),
                      columns = ['XV Average', 'Test'])

    # Normalized importance measures of the variables, in the pruned model
    # (empty = variable not in the pruned model)
    # (normalization = average importance equals 1)
    norm_importance = pd.DataFrame(data = np.nan,
                                   index = pd.MultiIndex.from_product( \
        [n_features_list, [i.__name__ for i in n_split_list],
         n_estim_list],
        names = ['n_features', 'n_split', 'n_estim']),
                                   columns = pd.MultiIndex.from_tuples( \
        pred_list))

    #
    wd, met_data = load_training_data(path_out, 'wd', prv, max_lag)

    # Separate the training and validation dataset
    X = met_data.values
    y = wd.values
    X_train, X_test, y_train, y_test = train_test_split( \
        X, y, test_size = pct, random_state = tv_seed)

    # Fit model and extract performance
    start = time()
    for i,j,k in it.product(n_features_list, n_split_list,
                            n_estim_list):
        if j(i) > X.shape[1]:
            continue

        reg = RandomForestRegressor(n_estimators = k,
                                    max_features = j(i),
                                    random_state = rf_seed)
        # Get the initial importance
        reg = reg.fit(X_train, y_train)
        which = np.argsort(reg.feature_importances_)[-i:]

        reg = RandomForestRegressor(n_estimators = k,
                                    max_features = j(i),
                                    random_state = rf_seed + i)
        cvs = cross_val_score(reg, X_train[:, which], y_train, cv = nfold,
                              scoring = 'r2')
        r2.loc[(i, j.__name__, k), 'XV Average'] = np.mean(cvs)

        # Get the afterwards importance
        reg = reg.fit(X_train[:, which], y_train)
        norm_importance.loc[(i, j.__name__, k),
                            [pred_list[n] for n in which]] = \
                                reg.feature_importances_ / \
                                np.mean(reg.feature_importances_)

        # Get the test dataset performance: R2 score
        r2.loc[(i, j.__name__, k),
               'Test'] = reg.score(X_test[:, which], y_test)
        end = time()
        ##print((end - start) / 60) # minutes

    r2.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                           'part1', 'wd_r2_' + prv + '.csv'))
    norm_importance.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                        'part1',
                                        'wd_norm_importance_' + prv + '.csv'))
    print(prv + ' success!')


p = mp.Pool(10)
p.map_async(calc, province)
p.close()
p.join()
