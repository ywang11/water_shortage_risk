import pandas as pd
import os
import numpy as np
from .constants import season_list2


def parse_key(string):
    """
    Parse the string into {Number: Province} dictionary.
    """
    num_to_prov = dict([(int(x.split(': ')[0]), x.split(': ')[1]) \
                        for x in string.split('; ')])
    prov_to_num = dict([(x.split(': ')[1], int(x.split(': ')[0])) \
                        for x in string.split('; ')])
    return num_to_prov, prov_to_num


def decode_time(time_since):
    if 'day' in time_since.attrs['units']:
        time = pd.date_range(start = time_since.attrs['units'].split(' ')[-2],
                             periods = int(time_since[-1]) + 1, freq = '1D')
        time = time[time_since.values.astype(int)]
    elif 'month' in time_since.attrs['units']:
        time = pd.date_range(start = time_since.attrs['units'].split(' ')[-2],
                             periods = int(time_since[-1]) + 1, freq = '1M')
        time = time[time_since.values.astype(int)]
    return time


def load_training_data(path_out, var, pr, max_lag):
    """
    Read the predictors and predictand data.
    Limit to after 1920 because of bad data quality in Gansu and Qinghai.
    """
    ## (1) Ocean indices
    #inx = pd.read_csv(os.path.join(path_out, 'inx_lagged.csv'),
    #                  index_col = 0).loc[ymin:ymax, :]
    goal = load_goal(path_out, var, pr)
    if var == 'wd':
        met_data = load_met(path_out, 'cru', pr, max_lag)
    else:
        met_data = load_met(path_out, 'isimip', pr, max_lag)
    met_data = met_data.iloc[(met_data.index >= goal.index[0]) & \
                             (met_data.index <= goal.index[-1]), :]
    if var == 'wsi':
        # WSI & Meteorological data shift models to rows
        goal = goal.stack().sort_index()
        met_data.columns = met_data.columns.reorder_levels([0,2,3,1])
        met_data = met_data.stack(dropna = False).sort_index()
    return goal, met_data


def load_goal(path_out, var, pr):
    if var == 'wd':
        goal = pd.read_csv(os.path.join(path_out, 'prv_PIrrWN_wd.csv'),
                           index_col = 0, parse_dates = True).loc[:, pr]
    else:
        goal = pd.read_csv(os.path.join(path_out, 'prv_wsi.csv'),
                           index_col = 0, header = [0,1],
                           parse_dates = True).loc[:, pr]
    goal.index = goal.index.year
    goal = goal.loc[goal.index >= 1920]
    return goal


def load_met(path_out, which, pr, max_lag):
    """
    Read the predictors and predictand data.
    """
    ## (1) Ocean indices
    ##inx = pd.read_csv(os.path.join(path_out, 'inx_lagged.csv'),
    ##                  index_col = 0).loc[ymin:ymax, :]
    # (2) Meteorological predictors
    if which == 'cru':
        met_data = pd.read_csv(os.path.join(path_out, 'met_province',
                                            'met_cru.csv'),
                               index_col = 0,
                               header = [0,1,2]).loc[:, (pr, slice(None),
                                                         season_list2)]
    else:
        met_data = pd.read_csv(os.path.join(path_out, 'met_province',
                                            'met_isimip.csv'),
                               header = [0,1,2,3])
        met_data.index = met_data.iloc[:, 0]
        met_data = met_data.iloc[:, 1:]
        met_data.index.name = ''
        met_data = met_data.loc[:, (pr, slice(None), slice(None),
                                    season_list2)]
    # ----- create lags
    temp = {}
    for lag in range(max_lag+1):
        temp['Lag '+str(lag)] = met_data.copy(deep = True)
        if lag > 0:
            temp['Lag '+str(lag)].iloc[lag:, :] = \
                temp['Lag '+str(lag)].iloc[:(-lag), :].values
            temp['Lag '+str(lag)].iloc[:lag, :] = np.nan
    met_data = pd.concat(temp)
    met_data.index = met_data.index.reorder_levels([1,0])
    met_data = met_data.sort_index().unstack()
    met_data.columns = met_data.columns.droplevel(0).remove_unused_levels()

    met_data = met_data.loc[met_data.index >= 1920, :]
    return met_data
