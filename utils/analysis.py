import numpy as np
from scipy.stats import pearsonr, linregress
import xarray as xr
import pandas as pd
from .paths import path_out
import os


def pred_ints(model, X, percentile=95):
    """
    Generate uncertainty interval for Random Forest.
    http://blog.datadive.net/prediction-intervals-for-random-forests/
    Reference: Meinshausen (2006), Quantile Regression Forests
    """
    tree_num = len(model.estimators_)
    preds = np.array([model.estimators_[i].predict(X) \
                      for i in range(tree_num)])
    err_down = np.percentile(preds, (100-percentile)/2., axis=0)
    err_up = np.percentile(preds, 100-(100-percentile)/2., axis=0)

    return err_down, err_up, preds


def gen_stats(obs, pred):
    rmse = np.sqrt(np.mean((obs - pred) * (obs - pred)))
    corr, corr_p = pearsonr(obs, pred)
    return rmse, corr, corr_p


def gen_stats2(obs, pred):
    temp = np.isnan(obs) | np.isnan(pred)
    obs = obs[~temp]
    pred = pred[~temp]

    bias = np.mean(pred) - np.mean(obs)
    corr, corr_p = pearsonr(obs, pred)
    rmse = np.sqrt(np.mean((obs - pred) * (obs - pred)))
    rmse_r = rmse / np.std(obs)
    return bias, corr, corr_p, rmse, rmse_r


def one_func(vector):
   result = linregress(np.arange(len(vector)),
                       vector)
   return result.slope


def detrend(sst):
   """ Remove the linear trend from each grid."""
   trend = xr.apply_ufunc(one_func,
                          sst, input_core_dims = [['time']],
                          vectorize = True, dask = 'allowed')
   trend = np.broadcast_to((np.arange(sst.shape[0]) - \
                            (sst.shape[0] - 1)/2).reshape(-1,1,1),
                           sst.shape) * trend.values[np.newaxis, :]
   trend = xr.DataArray(trend, dims = sst.dims,
                        coords = sst.coords)
   #print(trend)
   sst = sst - trend
   return sst, trend


def detrend_2d(met):
    """ Remove the linear trend from each column."""
    trend = met.apply(one_func, axis = 0, raw = True, 
                      result_type = 'broadcast')
    trend = np.matmul(np.diag(np.arange(trend.shape[0])),
                      trend)
    met = met - trend
    return met, trend


def deseasonalize(da):
    """ da = da_new + seasonality """
    seasonality_simple = da.groupby('time.quarter').mean()

    da_new = np.full_like(da.values, np.nan)
    seasonality = np.full_like(da_new, np.nan)

    tvec = da['time'].to_index()
    for qtr in [1,2,3,4]:
        da_new[tvec.quarter == qtr, :] = \
            da[tvec.quarter == qtr, :] - \
            seasonality_simple.loc[qtr, :]
        seasonality[tvec.quarter == qtr, :] = \
            seasonality_simple.loc[qtr, :]
    da_new = xr.DataArray(da_new,
                          dims = da.dims,
                          coords = da.coords)
    seasonality = xr.DataArray(seasonality,
                               dims = da.dims,
                               coords = da.coords)
    return da_new, seasonality


def deseasonalize_2d(df):
    """ df = df_new + seasonality """
    seasonality_simple = df.groupby(df.index.quarter).mean()

    df_new = np.full_like(df.values, np.nan)
    seasonality = np.full_like(df_new, np.nan)

    for qtr in [1,2,3,4]:
        df_new[df.index.quarter == qtr, :] = \
            df.loc[df.index.quarter == qtr, :] - \
            seasonality_simple.loc[qtr, :].values
        seasonality[df.index.quarter == qtr, :] = \
            seasonality_simple.loc[qtr, :]
    df_new = pd.DataFrame(df_new,
                          index = df.index, columns = df.columns)
    seasonality = pd.DataFrame(seasonality,
                               index = df.index, columns = df.columns)
    return df_new, seasonality


def get_pr_subset(var):
    performance = pd.read_csv((os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part2',
                                            'performance.csv')),
                              index_col=0,
                              header=[0, 1])
    return sorted(performance.index[((performance.loc[:,(var,'pVal')] <= 0.05) & \
                                     (performance.loc[:,(var,'R2')] > 0))])
