"""
20190910

ywang254@utk.edu
"""
import os
import platform

if platform.uname().node == '46T27S2':
    path_data = '/mnt/c/Users/ywang254/OneDrive - University of Tennessee/' + \
        'Projects/2018 Water Shortage Risk/data/'
    path_out = '/mnt/c/Users/ywang254/OneDrive - University of Tennessee/' + \
        'Projects/2018 Water Shortage Risk/output/'
elif platform.uname().node == 'DESKTOP-UC0OK7B':
    path_data = 'C:/Users/ywang254/OneDrive - University of Tennessee/Projects/2018 Water Shortage Risk/data'
    path_out = 'C:/Users/ywang254/OneDrive - University of Tennessee/Projects/2018 Water Shortage Risk/output'
else:
    path_data = os.path.join(os.environ['PROJDIR'], 'Water_Shortage_Risk', 'data')
    path_out = os.path.join(os.environ['PROJDIR'], 'Water_Shortage_Risk', 'output')
