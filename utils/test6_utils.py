# Implement the Probabilistic Gaussian Copula Regression model in
# https://journals.ametsoc.org/jcli/article/27/9/3331/35175
# using https://pyro.ai/examples/mle_map.html
import numpy as np
from scipy.stats import norm
import torch
from torch.distributions import constraints
import pyro
import pyro.distributions as dist
from pyro.infer import SVI, Trace_ELBO
##from pyro.contrib import gp
from sklearn.ensemble import RandomForestRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, RationalQuadratic, \
    ConstantKernel as C
pyro.enable_validation(True)    # <---- This is always a good idea!
import matplotlib.pyplot as plt
from .analysis import pred_ints
import itertools as it


class GMarginal:

    def __init__(self, ivec, mvec):
        self.ivec = ivec
        self.mvec = mvec
        self.e1 = np.nan
        self.e2 = np.nan
        self.e3 = np.nan
        self.loss = np.nan

    def gaussian_marginal(self):
        e1 = pyro.param('e1', torch.tensor(0.))
        e2 = pyro.param('e2', torch.tensor(np.mean(self.mvec)))
        e3 = pyro.param('e3', torch.tensor(np.std(self.mvec)))
        ##mean = e1 * torch.tensor(np.sign(self.ivec) * \
        ##                         np.log(np.abs(self.ivec)+1)) + e2
        mean = e1 * torch.tensor(self.ivec) + e2
        sigma = torch.exp( e3 * torch.tensor(self.ivec) )
        with pyro.plate('data', len(self.ivec)):
            pyro.sample('obs', dist.Normal(mean, sigma),
                        obs = torch.tensor(self.mvec))

    def guide(self):
        pass

    def train(self, lr = 0.005):
        pyro.clear_param_store()
        adam = pyro.optim.Adam({"lr": lr})
        svi = SVI(self.gaussian_marginal, self.guide, adam, loss=Trace_ELBO())

        n_steps = 4001
        for step in range(n_steps):
            loss = svi.step()
            if step % 50 == 0:
                print('[iter {}]  loss: {:.4f}'.format(step, loss))

        self.e1 = float(pyro.param('e1'))
        self.e2 = float(pyro.param('e2'))
        self.e3 = float(pyro.param('e3'))
        self.loss = float(loss)

        return self.e1, self.e2, self.e3, self.loss

    def predict(self, new_ivec, add_noise = False, seed = 999):
        self.new_ivec = new_ivec
        ##self.new_mvec = np.array([norm.rvs(self.e1*np.sign(ni)* \
        ##                                   np.log(np.abs(ni)+1) + self.e2,
        ##                                   np.exp(self.e3*ni),
        ##                                   random_state = seed) \
        ##                          for ni in self.new_ivec])
        if not add_noise:
            self.new_mvec = np.array([self.e1*ni + self.e2 for \
                                      ni in self.new_ivec])
        else:
            self.new_mvec = np.array([norm.rvs(self.e1*ni + self.e2,
                np.exp(self.e3*ni), random_state = seed + nind) \
                for nind, ni in enumerate(self.new_ivec)])
        return self.new_mvec


class GRegression:

    def __init__(self, ivec, mvec):
        self.ivec = ivec
        self.mvec = mvec
        self.e1 = np.nan
        self.e2 = np.nan
        self.e3 = np.nan
        self.loss = np.nan

    def gaussian_regression(self):
        e1 = pyro.param('e1', torch.tensor(0.))
        e2 = pyro.param('e2', torch.tensor(np.mean(self.mvec)))
        e3 = pyro.param('e3', torch.tensor(np.std(self.mvec)),
                        constraint = constraints.positive)
        mean = e1 * torch.tensor(self.ivec) + e2
        ##mean = e1 * torch.tensor(np.sign(self.ivec) * \
        ##                         np.log(np.abs(self.ivec)+1)) + e2
        with pyro.plate('data', len(self.ivec)):
            pyro.sample('obs', dist.Normal(mean, e3),
                        obs = torch.tensor(self.mvec))

    def guide(self):
        pass

    def train(self, lr = 0.005):
        pyro.clear_param_store()
        adam = pyro.optim.Adam({"lr": lr})
        svi = SVI(self.gaussian_regression, self.guide, adam,
                  loss=Trace_ELBO())

        n_steps = 4001
        for step in range(n_steps):
            loss = svi.step()
            if step % 50 == 0:
                print('[iter {}]  loss: {:.4f}'.format(step, loss))

        self.e1 = float(pyro.param('e1'))
        self.e2 = float(pyro.param('e2'))
        self.e3 = float(pyro.param('e3'))
        self.loss = float(loss)

        return self.e1, self.e2, self.e3, self.loss

    def predict(self, new_ivec, add_noise = False, seed = 999):
        self.new_ivec = new_ivec
        ##self.new_mvec = np.array([norm.rvs(self.e1*np.sign(ni)* \
        ##                                   np.log(np.abs(ni)+1) + self.e2,
        ##                                   self.e3,
        ##                                   random_state = seed) \
        ##                          for ni in self.new_ivec])
        if not add_noise:
            self.new_mvec = np.array([self.e1*ni + self.e2 for ni in \
                                      self.new_ivec])
        else:
            self.new_mvec = np.array([norm.rvs(self.e1*ni + self.e2, self.e3,
                                               random_state = seed + nind) \
                                      for nind,ni in enumerate(self.new_ivec)])
        return self.new_mvec


class GProcess:
    # http://pyro.ai/examples/gp.html

    def __init__(self, ivec, mvec):
        self.ivec = ivec
        self.mvec = mvec

        self.e1 = 1.
        self.e2 = np.std(self.ivec)
        self.loss = np.nan

        #self.kernel = gp.kernels.RBF(input_dim = 1,
        #                             variance = torch.tensor(self.e1),
        #                             lengthscale = torch.tensor(self.e2))
        #self.gpr = gp.models.GPRegression(torch.tensor(self.ivec),
        #                                  torch.tensor(self.mvec),
        #                                  self.kernel,
        #                                  noise = torch.tensor(self.e3))

        # RationalQuadratic kernel looks slightly better than RBF,
        # and constant seems uncessary
        # C(1.0, (1e-6, 1e6)) + 1.0 * RBF(1., (1e-6, 1e6))

        min_length = np.min(np.diff(np.unique(self.ivec))) * 1.1
        max_length = np.max(self.ivec) - np.min(self.ivec) * 0.9
        self.kernel = RationalQuadratic(alpha = self.e1,
                                        length_scale = self.e2,
                                        length_scale_bounds = (min_length,
                                                               max_length))
        self.gpr = GaussianProcessRegressor(kernel=self.kernel, alpha = 1e-5,
                                            n_restarts_optimizer=10,
                                            normalize_y = True)


    def train(self):
        #pyro.set_rng_seed(0)
        #
        #optimizer = torch.optim.Adam(self.gpr.parameters(), lr = lr)
        #loss_fn = pyro.infer.Trace_ELBO().differentiable_loss
        #n_steps = 4001
        #for step in range(n_steps):
        #    optimizer.zero_grad()
        #    loss = loss_fn(self.gpr.model, self.gpr.guide)
        #    loss.backward()
        #    optimizer.step()
        #    if step % 50 == 0:
        #        print('[iter {}]  loss: {:.4f}'.format(step, loss))
        #
        #self.e1 = self.gpr.kernel.variance.item()
        #self.e2 = self.gpr.kernel.lengthscale.item()
        #self.e3 = self.gpr.noise.item()
        #self.loss = float(loss)
        #return self.e1, self.e2, self.e3, self.loss
        np.random.seed(999)
        self.gpr.fit(self.ivec.reshape(-1,1), self.mvec)

        params = self.gpr.kernel_.get_params()

        self.e1 = params['alpha']
        self.e2 = params['length_scale']
        self.loss = self.gpr.log_marginal_likelihood_value_

        return self.e1, self.e2, self.loss


    #def plot(self, plot_observed_data=False, plot_predictions=False,
    #         n_prior_samples=0, n_test=500):
    #    pyro.set_rng_seed(5)
    #
    #    fig, ax = plt.subplots(figsize=(12, 6))
    #    if plot_observed_data:
    #        ax.plot(self.ivec, self.mvec, 'kx')
    #    if plot_predictions:
    #        Xtest = torch.linspace(np.min(self.ivec) - np.std(self.ivec)/10,
    #                               np.max(self.ivec) + np.std(self.ivec)/10,
    #                               n_test) # test inputs
    #        # compute predictive mean and variance
    #        with torch.no_grad():
    #            mean, cov = self.gpr(Xtest, full_cov=True, noiseless=False)
    #        # standard deviation at each input point x
    #        sd = cov.diag().sqrt()
    #
    #        # plot the mean
    #        ax.plot(Xtest.numpy(), mean.numpy(), 'r', lw=2)
    #        # plot the two-sigma uncertainty about the mean
    #        ax.fill_between(Xtest.numpy(),
    #                        (mean - 2.0 * sd).numpy(),
    #                        (mean + 2.0 * sd).numpy(),
    #                        color='C0', alpha=0.3)
    #    ax.xlim(Xtest.numpy()[0], Xtest.numpy()[-1])
    #    return fig, ax

    #def predict(self, new_ivec, add_noise = False, seed = 999):
    #    pyro.set_rng_seed(seed)
    #
    #    self.new_ivec = new_ivec
    #
    #    Xtest = torch.tensor(self.new_ivec)
    #
    #    if not add_noise:
    #        with torch.no_grad():
    #            mean, cov = self.gpr(Xtest, full_cov=True, noiseless=False)
    #        self.new_mvec = mean.numpy()
    #    else:
    #        n_test = len(self.new_ivec)
    #        n_prior_samples = 1
    #        #noise = (self.gpr.noise \
    #        #         if type(self.gpr) != gp.models.VariationalSparseGP
    #        #         else self.gpr.likelihood.variance)
    #        #cov = self.kernel.forward(Xtest) + noise.expand(n_test).diag()
    #        ##print(noise.type()) = torch.DoubleTensor
    #        ##print(cov.type())  = torch.DoubleTensor
    #        with torch.no_grad():
    #            mean, cov = self.gpr(Xtest, full_cov=True, noiseless=False)
    #        samples = dist.MultivariateNormal(torch.zeros(n_test),
    #                                          covariance_matrix=cov.float() \
    #        ).sample((n_prior_samples,))
    #        self.new_mvec = mean.numpy() + samples.numpy().reshape(-1)
    #
    #    return self.new_mvec

    def predict(self, new_ivec, add_noise = False, seed = 999):
        np.random.seed(seed)

        self.new_ivec = new_ivec
        self.new_mvec, \
            sigma_estim = self.gpr.predict(self.new_ivec.reshape(-1,1),
                                           return_std = True)
        if add_noise:
            noise = np.array([norm.rvs(0, ni,
                                       random_state = seed + nind) \
                              for nind,ni in enumerate(sigma_estim)])
            self.new_mvec = self.new_mvec + noise

        return self.new_mvec


class RF:
    # Add optimization to the random forest

    def __init__(self, ivec, mvec):
        self.ivec = ivec
        self.mvec = mvec

        self.e1 = 100. # n_estimators
        self.e2 = 'auto' # maximum features
        self.loss = np.nan

    def train(self):
        """
        Grid search using oob_score to optimize parameters.
        """
        n_estim_list = [4000,8000,12000]
        n_feat_list = ['auto', 'log2', 'sqrt']
        rf_seed = 999

        oob_error = np.full([len(n_estim_list), len(n_feat_list)], np.nan)
        for i,j in it.product(range(len(n_estim_list)),
                              range(len(n_feat_list))):
            reg = RandomForestRegressor(n_estimators = n_estim_list[i],
                                        max_features = n_feat_list[j],
                                        random_state = rf_seed,
                                        oob_score = True)
            reg = reg.fit(self.ivec, self.mvec)
            oob_error[i,j] = 1 - reg.oob_score_
        row, col = np.unravel_index(np.argmin(oob_error.reshape(-1)),
                                    oob_error.shape)

        self.e1 = n_estim_list[row]
        self.e2 = n_feat_list[col]

        self.rf = RandomForestRegressor(n_estimators = self.e1,
                                        max_features = self.e2,
                                        random_state = rf_seed,
                                        oob_score = True)
        self.rf = self.rf.fit(self.ivec, self.mvec)
        self.loss = 1 - self.rf.oob_score_

        return self.e1, self.e2, self.loss


    def predict(self, new_ivec, add_noise = False, seed = 999):
        self.new_ivec = new_ivec
        self.new_mvec = self.rf.predict(self.new_ivec)

        if add_noise:
            # randomly select on of the estimators
            i = np.random.randint(len(self.rf.estimators_))
            self.new_mvec = self.rf.estimators_[i].predict(self.new_ivec)

        return self.new_mvec

    
def get_best_model(var, m, ix):
    """
    Best models based on the "regression_both_test6_part3_step1_compare.py"
    script
    """
    if var == 'wd':
        if (ix == 'AMO') | (ix == 'IDMI'):
            # Best/Comparable performance, most concise model
            if m == 'pr':
                lag = 0
                regressor = GProcess
            else:
                lag = 0
                regressor = GRegression
        else:
            if ix == 'NAO':
                if (m == 'pet') | (m == 'pr'):
                    lag = 1
                    regressor = GMarginal
                else:
                    lag = 0
                    regressor = RF
            elif ix == 'PDO':
                if (m == 'pet'):
                    lag = 1
                    regressor = GProcess
                else:
                    lag = 1
                    regressor = RF
            elif ix == 'SOI':
                if m == 'pet':
                    lag = 1
                    regressor = GProcess
                else:
                    lag = 0
                    regressor = GRegression
    else:
        if (ix == 'AMO') | (ix == 'IDMI'):
            # Best/Comparable performance, most concise model
            if m == 'pr':
                lag = 0
                regressor = GProcess
            else:
                lag = 0
                regressor = GRegression
        else:
            if ix == 'NAO':
                if (m == 'pet') | (m == 'pr'):
                    lag = 1
                    regressor = GMarginal
                else:
                    lag = 0
                    regressor = RF
            elif ix == 'PDO':
                if (m == 'pet'):
                    lag = 1
                    regressor = GProcess
                else:
                    lag = 1
                    regressor = RF
            elif ix == 'SOI':
                if m == 'pet':
                    lag = 1
                    regressor = GProcess
                else:
                    lag = 0
                    regressor = GRegression
    return lag, regressor
