import torch
from torch import nn
import numpy as np


class Model(nn.Module):
    def __init__(self, input_size, output_size, hidden_dim, n_layers,
                 device):
        super(Model, self).__init__()

        self.device = device

        # Defining some parameters
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers

        # Defining the layers
        # RNN Layer
        self.rnn = nn.RNN(input_size, hidden_dim, n_layers, batch_first=False)

        # Fully connected layer
        self.fc = nn.Linear(hidden_dim, output_size)

    def forward(self, x):
        
        batch_size = x.size(1)

        #Initializing hidden state for first input using method defined below
        hidden = self.init_hidden(batch_size)

        # Passing in the input and hidden state into the model and
        # obtaining outputs
        out, hidden = self.rnn(x, hidden)

        # Reshaping the outputs such that it can be fit into the fully
        # connected layer
        out = out.contiguous().view(-1, self.hidden_dim)
        out = self.fc(out)
        
        return out, hidden
    
    def init_hidden(self, batch_size):
        # This method generates the first hidden state of zeros which we'll
        # use in the forward pass
        hidden = torch.zeros(self.n_layers, batch_size,
                             self.hidden_dim, dtype = torch.float64 \
        ).to(self.device)
        # We'll send the tensor holding the hidden state to the device
        # we specified earlier as well
        return hidden


def get_device():
    is_cuda = torch.cuda.is_available()

    # If we have a GPU available, we'll set our device to GPU. We'll use
    # this device variable later in our code.
    if is_cuda:
        device = torch.device("cuda")
        print("GPU is available")
    else:
        device = torch.device("cpu")
        print("GPU not available, CPU used")

    return device


def train(x_train, y_train):
    # Create an instance of the RNN model
    device = get_device()
    model = Model(input_size = x_train.shape[-1], output_size = 1,
                  hidden_dim = 2, n_layers = 2, device = device)
    model = model.to(device).double()

    # Some hyperparameters
    n_epoches = 400
    lr = 0.005

    # More settings
    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    #
    x_train = torch.from_numpy(x_train).to(device)
    y_train = torch.from_numpy(y_train)

    for epoch in range(1, n_epoches + 1):
        optimizer.zero_grad()
        output, hidden = model(x_train)
        output = output.to(device)
    
        loss = criterion(output, y_train.view(-1,1))
        loss.backward()
        optimizer.step()

        if (epoch > 300) & (epoch%10 == 0):
            print('Epoch: {}/{}.............'.format(epoch, n_epoches),
                  end=' ')
            print("Loss: {:.4f}".format(loss.item()))

    return model, loss


def predict(model, x_test, y_test):
    is_cuda = torch.cuda.is_available()
    if is_cuda:
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")
    
    x_test = torch.from_numpy(x_test).to(device)
    y_test = torch.from_numpy(y_test)

    out, h = model(x_test)
    criterion = nn.MSELoss()
    loss = criterion(out, y_test.view(-1,1))

    print('Out-of-sample: .............', end = ' ')
    print('Loss: {:.4f}'.format(loss.item()))
    return out, h, loss
