import numpy as np
import xarray as xr
import os
from scipy.stats import norm
from statsmodels.tsa.stattools import acf
from statsmodels.regression.linear_model import OLS
from statsmodels.tools.tools import add_constant
from statsmodels.sandbox.regression.predstd import wls_prediction_std
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from scipy.stats import pearsonr
import geopandas as gpd
from .paths import *


def stipple(ax, lat, lon, p_values, thresh=0.05, transform=None,
            hatch = '//////'):
    """
    Stipple points using plt.scatter for values below thresh.
    """
    xData, yData = np.meshgrid(lon, lat)
    ax.contourf(xData, yData, p_values < thresh, 1,
                hatches = ['', hatch], transform = transform,
                alpha = 0, zorder = 3)


def plot_province(ax, vector, cmap, norm, orientation = 'vertical'):
    df = gpd.read_file(os.path.join(path_data, 'China_Provinces', 'China-provincial_level.shp'))
    df.set_index('ID', inplace = True)
    df.sort_index(inplace = True)

    df.plot(ax = ax, facecolor = '#FFFFFF', edgecolor = 'k', linewidths = 0.5)

    df3 = gpd.read_file(os.path.join(path_data, 'China_Provinces', 'South_sea_boundry.shp'))
    df3.plot(ax = ax, facecolor = 'none', edgecolor = 'k', linewidths = 0.5)

    ylim0 = ax.get_ylim()
    ax.set_ylim([ylim0[0] + 17, ylim0[1]])

    df2 = df.loc[vector.index, :]
    df2['values'] = vector.values

    df2.plot(column = df2['values'], ax = ax, legend = False, cmap = cmap, norm = norm, edgecolor = 'none', linewidths = 0.5)

    cbar = plt.cm.ScalarMappable(norm = norm, cmap = cmap)
    plt.colorbar(cbar, ax = ax, aspect = 50, shrink = 0.7, pad = 0.08, orientation = orientation)

    pos = ax.get_position().bounds # left, bottom, width, height
    pos2 = [pos[0] + pos[2] * 0.76, pos[1] + pos[3] * 0.03, pos[2] * 0.2, pos[3] * 0.3]
    ax_inset = ax.get_figure().add_axes(pos2, projection = ccrs.PlateCarree())
    df.plot(ax = ax_inset, facecolor = '#FFFFFF', edgecolor = 'k', linewidths = 0.5)
    df2.plot(column = df2['values'], ax = ax_inset, legend = False, cmap = cmap, norm = norm, edgecolor = 'none', linewidths = 0.5)
    df3.plot(ax = ax_inset, facecolor = 'none', edgecolor = 'k', linewidths = 0.5)
    ax_inset.set_ylim([ylim0[0], ylim0[1]-30])
    ax_inset.set_xlim([105, 126])


def ax_histogram(ax, vector, bins, dist = 'norm',
                 show = 'both',
                 args_hist = {'color': '#bdbdbd', 'edgecolor': '#636363'},
                 args_line = {'color': '#2b8cbe'}):
    """
    Plot a histogram in panel with fitted Gaussian distribution.
    
    Parameters
    ----------
      vector: 1-d array
          The data set.
      bins: int or a sequence of increasing numbers
          The same as input into **numpy.histogram**. 
          Defines the number of bins or the bin edges, from left-most to 
          right-most.
      dist: str
          If *norm*, plot fitted normal distribution. 
          If *kde*, plot fitted Gaussian kernal density.
      show: str
          If *both* (default), plot both the histogram bars and fitted
          distribution.
          If *bar*, plot only the histogram bars.
          If *line*, plot only the fitted distribution using lines.
    """
    vector = vector[~np.isnan(vector)]
    hist, bin_edges = np.histogram(vector, bins = bins, density = True)

    h = []
    
    if (show == 'bar') | (show == 'both'):
        h1 = ax.bar(bin_edges[:-1], hist, 
                    width = bin_edges[1:] - bin_edges[:-1],
                    align = 'edge', **args_hist)
        h.append(h1)

        # convenience to insure edge is not transparent.
        if 'alpha' in args_hist:
            del args_hist['alpha']
            args_hist['facecolor'] = 'None'
            ax.bar(bin_edges[:-1], hist,
                   width = bin_edges[1:] - bin_edges[:-1],
                   align = 'edge', **args_hist)

    if (show == 'line') | (show == 'both'):
        x = np.linspace(bin_edges[0], bin_edges[-1], 100)

        if dist == 'norm':
            mean, std = norm.fit(vector)
            h2, = ax.plot(x, norm.pdf(x, mean, std), **args_line)
        elif dist == 'kde':
            kde = KernelDensity(bandwidth = np.std(vector)/5,
                                kernel='gaussian')
            kde.fit(vector)
            prob = np.exp(kde.score_samples(x))
            h2, = ax.plot(x, prob, '-', **args_line)
        h.append(h2)

    return h


def panel_acf(ax, vector, max_lag = 12,
              args_bar = {'color': '#bdbdbd', 'edgecolor': '#636363'},
              args_ci = {'color': 'k'}):
    """
    Plot the autocorrelation function of the time series.
    """
    rho, confint, qstat, pvalues = \
        acf(vector[~np.isnan(vector)],
            unbiased = True, nlags = max_lag, qstat = True,
            alpha = 0.05)

    x = range(1, max_lag + 1)
    ax.bar(x, rho[1:], **args_bar)
    ax.errorbar(x, rho[1:], confint.T[:, 1:] - rho[1:], **args_ci)


def ppp(p_value):
    if p_value <= 0.01:
        return '$^{***}$'
    elif p_value <= 0.05:
        return '$^{**}$'
    elif p_value <= 0.1:
        return '$^{*}$'
    else:
        return ''

    
def ppf(slope, intercept, p_slope, p_intercept):
    if (np.abs(slope) < 1e-1) | (np.abs(slope) > 10):
        fslope = '{:.2e}'.format(slope)
    else:
        fslope = '%.2f' % slope
    fslope += ppp(p_slope)

    if (np.abs(intercept) < 1e-1) | (np.abs(intercept) > 10):
        fintercept = '{:.2e}'.format(np.abs(intercept))
    else:
        fintercept = '%.2f' % (np.abs(intercept))
    if intercept > 0:
        fintercept = ' + ' + fintercept
    else:
        fintercept = ' - ' + fintercept
    fintercept += ppp(p_intercept)

    return 'y = ' + fslope + u'\u00d7' + 'x' + fintercept


def ax_regress(ax, x, vector, 
               display = 'equation',
               pos_xy = [0.1, 0.9],
               args_pt = {'ls': '-'},
               args_ln = {'color': 'k'},
               args_ci = {'color': 'k', 'alpha': 0.2},
               args_tx = {'color': 'k'}):
    """
    Plot the time series with trend.
    Parameters
    ----------
    ax: matplotlib.pyplot.axis
    x: 1-d array
        The x-values in the regression.
    vector: 1-d array
        The y-values in the regression.
    display: None or str
        If None, does not display the regression equation.
        If 'equation', display the regression equation.
        If 'pearson', display the Pearson correlation.
    pos_xy: [float, float]
        The position to place the annotation in the normalized axis unit.
    args_pt, args_ln, args_ci, args_txt: dict
        Keyword arguments to be passed into the scatter plot, regression
        line, confidence interval for the regression line, and annotation
        text plotting functions.
    """
    temp = (~np.isnan(vector)) & (~np.isnan(x))
    x = x[temp]
    vector = vector[temp]

    ax.plot(x, vector, **args_pt)

    reg = OLS(vector, add_constant(x)).fit()
    ax.plot(x, x * reg.params[1] + reg.params[0], **args_ln)

    _, predict_ci_low, predict_ci_upp = wls_prediction_std(reg, \
        exog = reg.model.exog, weights = np.ones(len(reg.model.exog)))
    x_ind = np.argsort(x)
    ax.fill_between(x[x_ind], predict_ci_low[x_ind], 
                    predict_ci_upp[x_ind], interpolate = True,
                    **args_ci)

    if display == 'equation':
        ax.text(pos_xy[0], pos_xy[1],
                ppf(reg.params[1], reg.params[0],
                    reg.pvalues[1], reg.pvalues[0]),
                transform = ax.transAxes, **args_tx)
    elif display == 'pearson':
        r, pval = pearsonr(x, vector)
        ax.text(pos_xy[0], pos_xy[1],
                ('%.3f' % r) + ppp(pval),
                transform = ax.transAxes, **args_tx)
