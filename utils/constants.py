import pandas as pd


model = ['gfdl-esm2m', 'hadgem2-es', 'ipsl-cm5a-lr', 'miroc-esm-chem',
         'noresm1-m']
province = ['Anhui', 'Beijing', 'Chongqing', 'Fujian', 'Gansu',
            'Guangdong', 'Guangxi', 'Guizhou', 'Hainan',
            'Hebei', 'Heilongjiang', 'Henan', 'Hubei',
            'Hunan', 'Jiangsu', 'Jiangxi', 'Jilin', 
            'Liaoning', 'Inner Mongolia', 'Ningxia',
            'Qinghai', 'Shaanxi', 'Shandong', 'Shanghai', 'Shanxi', 
            'Sichuan', 'Tianjin', 'Xinjiang', 'Yunnan', 'Zhejiang']
wsi_period = pd.date_range('1971-01-01', '2004-12-31', freq = 'YS')
met = ['pet', 'pr', 'tas', 'tasmax', 'tasmin']
xclim = ['AMO', 'IDMI', 'NAO', 'PDO', 'SOI']
#plist = ['10', '20', '30'] # percentage area for spearman correlation
season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']
season_list2 = ['DJF', 'MAM', 'JJA', 'SON']


# WSI of "observed"
WSI0 = {'Anhui': 0.03, 'Beijing': 1.00, 'Chongqing': 0.02, 'Fujian': 0.18,
        'Gansu': 0.89, 'Guangdong': 0.11, 'Guangxi': 0.03, 'Guizhou': 0.02,
        'Hainan': 0.03, 'Hebei': 1.00, 'Heilongjiang': 0.12,
        'Henan': 0.61, 'Hubei': 0.03, 'Hunan': 0.03, 'Jiangsu': 0.94,
        'Jiangxi': 0.03, 'Jilin': 0.13, 'Liaoning': 0.58,
        'Inner Mongolia': 0.66, 'Ningxia': 0.99, 'Qinghai': 0.67,
        'Shaanxi': 0.69, 'Shandong': 1.00, 'Shanghai': 1.00, 'Shanxi': 1.00,
        'Sichuan': 0.10, 'Tianjin': 1.00, 'Xinjiang': 0.96,
        'Yunnan': 0.03, 'Zhejiang': 0.47}


# WD of "observed", but in the unit of cubic meters
WD0 = {'Anhui': 16670000000, 'Beijing': 1080000000, 'Chongqing': 1980000000,
       'Fujian': 9720000000, 'Gansu': 9430000000, 'Guangdong': 22750000000,
       'Guangxi': 19460000000, 'Guizhou': 5000000000, 'Hainan': 3390000000,
       'Hebei': 14380000000, 'Heilongjiang': 24960000000, 'Henan': 12560000000,
       'Hubei': 13830000000, 'Hunan': 18580000000,
       'Inner Mongolia': 13450000000, 'Jiangsu': 30420000000,
       'Jiangxi': 15100000000, 'Jilin': 7380000000,
       'Liaoning': 8980000000, 'Ningxia': 6500000000, 'Qinghai': 2320000000,
       'Shaanxi': 5550000000, 'Shandong': 15480000000,
       'Shanghai': 1680000000, 'Shanxi': 3800000000, 'Sichuan': 12730000000,
       'Tianjin': 1100000000, 'Xinjiang': 48460000000, 'Yunnan': 9530000000,
       'Zhejiang': 9460000000}


# Output of the Ag sector in 10,000 RMB
AgO = {'Anhui': 34966513, 'Beijing': 3711704, 'Chongqing': 13149593,
       'Fujian': 28206139, 'Gansu': 12480888, 'Guangdong': 43676184,
       'Guangxi': 32739150, 'Guizhou': 13473974, 'Hainan': 10149389,
       'Hebei': 50084420, 'Heilongjiang': 37068283, 'Henan': 62642126,
       'Hubei': 44382135, 'Hunan': 45995120, 'Inner Mongolia': 22972103,
       'Jiangsu': 54034621, 'Jiangxi': 22502431, 'Jilin': 23466264,
       'Liaoning': 38007383, 'Ningxia': 3617747, 'Qinghai': 2474720,
       'Shaanxi': 21601538, 'Shandong': 74522487, 'Shanghai': 3017477,
       'Shanxi': 12232539, 'Sichuan': 50956753, 'Tianjin': 3523001,
       'Xinjiang': 21343332, 'Yunnan': 25144858, 'Zhejiang': 24935430}
