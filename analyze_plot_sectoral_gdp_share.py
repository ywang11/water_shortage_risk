import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from cycler import cycler
from utils.paths import *


region_group = {'North': ['Beijing', 'Tianjin', 'Hebei', 'Shanxi', 'Inner Mongolia'],
                'Northeast': ['Liaoning', 'Jilin', 'Heilongjiang'],
                'East': ['Shanghai', 'Jiangsu', 'Zhejiang', 'Anhui', 'Fujian', 'Jiangxi', 'Shandong'],
                'Central': ['Henan', 'Hubei', 'Hunan', 'Guangdong', 'Guangxi', 'Hainan'],
                'Southwest': ['Chongqing', 'Sichuan', 'Guizhou', 'Yunnan'],
                'Northwest': ['Shaanxi', 'Gansu', 'Qinghai', 'Ningxia', 'Xinjiang']}

colors_name_list = ['Greys', 'Purples', 'Blues', 'Oranges', 'Greens', 'RdPu']
colors = []
for i, name in enumerate(['North', 'Northeast', 'East', 'Central', 'Southwest', 'Northwest']):
    colors = colors + list(plt.cm.get_cmap(colors_name_list[i])(np.linspace(0.3, 1, len(region_group[name]))))


os.chdir('C:/Users/ywo/OneDrive - Oak Ridge National Laboratory/Projects/2018 Water Shortage Risk/output/risk_results/20220505')



# Sectoral GDP share in the provinces
data = pd.read_excel(os.path.join(path_out, 'risk_results', '20220505', 'sectoral gdp_3030.xlsx'), sheet_name = 'sectoral gdp_3030', index_col = 0)
sectors = data.columns

data = data / data.sum(axis = 0) * 100

data = data.T

mpl.rcParams['axes.prop_cycle'] = cycler(color = colors)

fig, ax = plt.subplots(figsize = (9, 8))
data.plot(kind = 'bar', ax = ax, stacked = True)
plt.legend(data.columns, ncol = 5, bbox_to_anchor = (1., -0.18))
ax.set_xticks(range(data.shape[0]))
ax.set_xticklabels(data.index, rotation = 90)
ax.set_ylim([0, 100])
ax.set_ylabel('Percentage output share (%)')
# cf = ax.imshow(data.T, cmap = 'Reds', norm = mpl.colors.LogNorm(vmin = 1, vmax = 35))
#ax.set_xticks(range(data.shape[1]))
#ax.set_xticklabels(data.columns, rotation = 90)
#ax.set_yticks(range(data.shape[0]))
#ax.set_yticklabels(data.index)
# plt.colorbar(cf, shrink = 0.7, orientation = 'horizontal', aspect = 50)
plt.savefig(os.path.join(path_out, 'risk_results', '20220505', 'sectoral_gdp_share.png'), dpi = 600., bbox_inches = 'tight')


# Provinces water use share in the sectors
data = pd.read_excel('../Fig4 rank_waterUse.xlsx', sheet_name = 'sortBySector', index_col = 0)
data = data.iloc[:30, :30]
data.index = sectors

data = data / data.sum(axis = 0) * 100
data = data.T

mpl.rcParams['axes.prop_cycle'] = cycler(color = colors)

fig, ax = plt.subplots(figsize = (9, 8))
data.plot(kind = 'bar', ax = ax, stacked = True)
plt.legend(data.columns, ncol = 5, bbox_to_anchor = (1., -0.18))
ax.set_xticks(range(data.shape[0]))
ax.set_xticklabels(data.index, rotation = 90)
ax.set_ylim([0, 100])
ax.set_ylabel('Percentage water withdrawal share (%)')
# cf = ax.imshow(data.T, cmap = 'Reds', norm = mpl.colors.LogNorm(vmin = 1, vmax = 35))
#ax.set_xticks(range(data.shape[1]))
#ax.set_xticklabels(data.columns, rotation = 90)
#ax.set_yticks(range(data.shape[0]))
#ax.set_yticklabels(data.index)
# plt.colorbar(cf, shrink = 0.7, orientation = 'horizontal', aspect = 50)
plt.savefig(os.path.join(path_out, 'risk_results', '20220505', 'provincial_gdp_share.png'), dpi = 600., bbox_inches = 'tight')
