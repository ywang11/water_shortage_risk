"""
2020/12/28
ywang254@utk.edu

Bias-correct the predicted WSI & WD values in part2.py by scaling the 2012
 value to match.
"""
import pandas as pd
import numpy as np
import os
from utils.paths import path_data, path_out
from utils.constants import *
from time import time
import itertools as it
import matplotlib.pyplot as plt
import matplotlib as mpl


for var in ['wd', 'wsi']:
    # read the predicted data
    data = pd.read_csv(os.path.join(path_out, 'regression',
                                    'wsi_wd_test6', 'part2', 
                                    'goal_' + var + '.csv'),
                       index_col = 0)
    data = data.loc[1920:, :]

    #
    if var == 'wsi':
        for pr in province:
            data.loc[:, pr] = data.loc[:, pr].fillna(WSI0[pr])
            data.loc[:, pr] = data.loc[:, pr] * WSI0[pr] / \
                data.loc[2012, pr]
        data = data.clip(0, 1)
    else:
        irr_area = pd.read_csv(os.path.join(path_out, 'prv_irr_luh2.csv'),
                               index_col = 0, header = 0).loc[2012, :]
        liv = pd.read_csv(os.path.join(path_out, 'prv_PLivWN.csv'),
                          index_col = 0, parse_dates = True)
        liv.index = liv.index.year.values
        liv = liv.loc[2012, :]
        for pr in province:
            # convert from thousand m3 per year per km2 to m3 per year per yuan
            data.loc[:, pr] = data.loc[:, pr] * irr_area.loc[pr] / \
                AgO[pr] / 10
            # total ag sector = irrigation + livestock
            data.loc[:, pr] += (liv.loc[pr] * 100 / AgO[pr])
            # scale by the actual values in 2012
            actual_2012 = WD0[pr] / AgO[pr] / 1e4

            print(data.loc[2012, pr] / actual_2012) # check scaling factor

            data.loc[:, pr] = data.loc[:, pr].fillna(actual_2012)
            data.loc[:, pr] = data.loc[:, pr] * actual_2012 / \
                data.loc[2012, pr]
        data = data.clip(0)

    #
    data.to_csv(os.path.join(path_out, 'regression', 
                             'wsi_wd_test6', 'part2',
                             'goal_' + var + '_bc.csv'))
