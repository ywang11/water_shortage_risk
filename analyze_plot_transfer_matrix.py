import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import BoundaryNorm, ListedColormap
from matplotlib.cm import get_cmap
from matplotlib import rcParams
from matplotlib.gridspec import GridSpec
import numpy as np
import os
import matplotlib.ticker as ticker
from utils.paths import *


def cmap_div(N):
    """
    Colormap with clear division between positive and negative values in the middle.
    """
    c = get_cmap('viridis', 256)
    new1 = c(np.linspace(0.2, 0.9, N // 2))
    c = get_cmap('plasma_r', 256)
    new2 = c(np.linspace(0.05, 0.8, N - N // 2))
    newcmp = ListedColormap(np.concatenate([new1, new2], axis = 0))
    return newcmp

name_list = {
    'prov': ['Beijing', 'Tianjin', 'Hebei', 'Shanxi', 'Inner Mongolia', 'Liaoning', 'Jilin', 'Heilongjiang', 'Shanghai', 'Jiangsu', 'Zhejiang', 'Anhui', 'Fujian', 'Jiangxi', 'Shandong', 'Henan', 'Hubei', 'Hunan', 'Guangdong', 'Guangxi', 'Hainan', 'Chongqing', 'Sichuan', 'Guizhou', 'Yunnan', 'Shaanxi', 'Gansu', 'Qinghai', 'Ningxia', 'Xinjiang'],
    'sector': ['Ag', 'CoalM', 'PetroGas', 'MetalM', 'NonM', 'Food', 'Tex', 'Cloth', 'Wood', 'PaperM', 'PetroRe', 'CheInd', 'NonP', 'Metallurgy', 'MetalP', 'GeSpe', 'TransE', 'ElectriEqui', 'ElectroEqui', 'Instru', 'OthManu', 'EleWaSu', 'GasWaSu', 'Con', 'TransS', 'WholR', 'Hot', 'LeaComS', 'SciR', 'OthSe']
}

region_group = {'North': ['Beijing', 'Tianjin', 'Hebei', 'Shanxi', 'Inner Mongolia'],
                'Northeast': ['Liaoning', 'Jilin', 'Heilongjiang'],
                'East': ['Shanghai', 'Jiangsu', 'Zhejiang', 'Anhui', 'Fujian', 'Jiangxi', 'Shandong'],
                'Central': ['Henan', 'Hubei', 'Hunan', 'Guangdong', 'Guangxi', 'Hainan'],
                'Southwest': ['Chongqing', 'Sichuan', 'Guizhou', 'Yunnan'],
                'Northwest': ['Shaanxi', 'Gansu', 'Qinghai', 'Ningxia', 'Xinjiang']}
region_group_reverse = dict([(c, a) for a, b in region_group.items() for c in b])

path_temp = os.path.join(path_out, 'risk_results', '20220505', 'Raw data for Fig. 4~9', '03 all transfer metrix (fig8~9)')


for name in ['prov', 'sector']:
    val_2012 = pd.read_csv(os.path.join(path_temp, f'VT_{name}_2012.csv'), header = None)
    val_mean = pd.read_csv(os.path.join(path_temp, f'VT_{name}_30_mean.csv'), header = None)
    val_SVD = {
        'SVD 1+': pd.read_csv(os.path.join(path_temp, f'VT_{name}_30_0p.csv'), header = None),
        'SVD 1-': pd.read_csv(os.path.join(path_temp, f'VT_{name}_30_0m.csv'), header = None),
        'SVD 2+': pd.read_csv(os.path.join(path_temp, f'VT_{name}_30_1p.csv'), header = None),
        'SVD 2-': pd.read_csv(os.path.join(path_temp, f'VT_{name}_30_1m.csv'), header = None)
    }
    lwsr_median = pd.read_csv(os.path.join(path_temp, f'../02 medians (fig4~7)/lwsr_med_{name}.csv'), index_col = 0)
    lwsr_median.set_index(['SVDs', 'region'], inplace = True)
    lwsr_median = lwsr_median['med'].unstack().T.loc[name_list[name], :]
    lwsr_median['SVD 1+ minus SVD 1-'] = lwsr_median['SVD 1+'] - lwsr_median['SVD 1-']
    lwsr_median['SVD 2+ minus SVD 2-'] = lwsr_median['SVD 2+'] - lwsr_median['SVD 2-']

    lab = 'abcdefghijklmn'
    rcParams['font.size'] = 5.5
    rcParams['axes.titlesize'] = 5.5
    if name == 'prov':
        boundaries = (-50, -25, -10, -5, -1, 1, 5, 10, 25, 50)
        # boundaries = (-1000, -500, -100, -50, -10, -5, 0, 5, 10, 50, 100, 500, 1000)
        extend = 'both'
    else:
        boundaries = (-50, -25, -10, -5, -1, 1, 5, 10, 25, 50)
        # boundaries = (-1000, -500, -100, -50, -10, -5, 0, 5, 10, 50, 100, 500, 1000)
        extend = 'both'
    norm = BoundaryNorm(boundaries = boundaries, ncolors = len(boundaries) + 1, clip = False, extend = 'both')
    cmap = get_cmap('seismic', len(boundaries) + 1)
    # cmap = cmap_div(len(boundaries) + 1)

    fig = plt.figure(figsize = (7.5, 9))
    gs = GridSpec(3, 4, fig, hspace = 0.1, wspace = 0.02, width_ratios = [30, 1, 30, 1], height_ratios = [1,1,1])

    for i, col in enumerate([1,2]):
        for j, row in enumerate([f'SVD {col}+', f'SVD {col}-', f'SVD {col}+ minus SVD {col}-']):

            if row == 'SVD 1+ minus SVD 1-':
                pct = (val_SVD['SVD 1+'] - val_SVD['SVD 1-']) / val_mean * 100.
            elif row == 'SVD 2+ minus SVD 2-':
                pct = (val_SVD['SVD 2+'] - val_SVD['SVD 2-']) / val_mean * 100.
            else:
                pct = (val_SVD[row] - val_2012) / val_mean * 100.

            # pct.index = name_list[name]
            # pct.columns = name_list[name]
            # print(pct.loc['Jiangsu', :].mean())

            ax = fig.add_subplot(gs[j, i*2])
            cf = ax.imshow(pct, norm = norm, cmap = cmap, aspect = 0.8)
            ax.set_title(row)
            ax.text(0., 1.02, lab[j*4 + i*2], fontweight = 'bold', transform = ax.transAxes)
            ax.set_xticks(range(30))
            ax.set_yticks(range(30))
            ax.tick_params('both', length = 0., width = 0.)

            if i == 0:
                ax.set_yticklabels(name_list[name])
            else:
                ax.set_yticklabels([])
            if j == 2:
                ax.set_xticklabels(name_list[name], rotation = 90)
            else:
                ax.set_xticklabels([])

            if j == 2 and name == 'prov':
                ax2 = fig.add_axes(ax.get_position())
                ax2.set_facecolor('none')
                ax2.spines['top'].set(visible = False)
                ax2.spines['right'].set(visible = False)
                ax2.spines['left'].set(visible = False)
                ax2.spines['bottom'].set_position(('axes', -0.35))
                ax2.tick_params('both', length = 0., width = 0., which = 'minor')
                ax2.tick_params('both', direction = 'in', which = 'major')
                ax2.xaxis.set_ticks_position('bottom')
                ax2.xaxis.set_label_position('bottom')
                ax2.set_yticks([])

                xticks = [0]
                for r, l in region_group.items():
                    xticks.append(xticks[-1] + len(l))
                xticks = np.array(xticks)
                xticks_mid = (xticks[1:] + xticks[:-1]) * 0.5

                ax2.set_xticks(xticks)
                ax2.xaxis.set_major_formatter(ticker.NullFormatter())
                ax2.xaxis.set_minor_locator(ticker.FixedLocator(xticks_mid))
                ax2.xaxis.set_minor_formatter(ticker.FixedFormatter(list(region_group.keys())))

            if i == 0 and name == 'prov':
                ax3 = fig.add_axes(ax.get_position())
                ax3.invert_yaxis()
                ax3.set_facecolor('none')
                ax3.spines['right'].set(visible = False)
                ax3.spines['top'].set(visible = False)
                ax3.spines['bottom'].set(visible = False)
                ax3.spines['left'].set_position(('axes', -0.28))
                ax3.tick_params('both', length = 0., width = 0., labelrotation = 75, which = 'minor')
                ax3.tick_params('both', direction = 'in', which = 'major')
                ax3.yaxis.set_ticks_position('left')
                ax3.yaxis.set_label_position('left')
                ax3.set_xticks([])

                yticks = [0]
                for r, l in region_group.items():
                    yticks.append(yticks[-1] + len(l))
                yticks = np.array(yticks)
                yticks_mid = (yticks[1:] + yticks[:-1]) * 0.5

                ax3.set_yticks(yticks)
                ax3.yaxis.set_major_formatter(ticker.NullFormatter())
                ax3.yaxis.set_minor_locator(ticker.FixedLocator(yticks_mid))
                ax3.yaxis.set_minor_formatter(ticker.FixedFormatter(list(region_group.keys())))
                plt.setp(ax3.yaxis.get_minorticklabels(), va = 'center')

            # Add an axes in the same colorbar, but showing the median LWSR percentages only
            ax = fig.add_subplot(gs[j, i*2+1])
            ax.imshow(lwsr_median[[row]], norm = norm, cmap = cmap)
            ax.tick_params('both', length = 0., width = 0.)
            ax.text(0., 1.02, lab[j*4 + i*2 + 1], fontweight = 'bold', transform = ax.transAxes)
            ax.set_xticks([])
            ax.set_yticks(range(30))
            ax.set_yticklabels([])

    if name == 'sector':
        cax = fig.add_axes([0.1, 0.02, 0.8, 0.01])
    else:
        cax = fig.add_axes([0.1, -0.02, 0.8, 0.01])
    plt.colorbar(cf, cax = cax, orientation = 'horizontal', ticks = boundaries)

    fig.savefig(os.path.join(path_temp, f'pct_change_{name}.png'), dpi = 600., bbox_inches = 'tight')
    plt.close(fig)