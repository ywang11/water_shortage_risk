"""
20210323
ywang254@utk.edu

Check the partial dependence between top five predictors and predictand.
"""
import pandas as pd
import numpy as np
import os
from utils.paths import path_data, path_out
from utils.constants import *
from utils.io import load_training_data, load_met
import matplotlib.pyplot as plt
import matplotlib as mpl
from time import time
import itertools as it
from joblib import load
from regression_both_test6_part2 import subset_predictors, min_max, max_max, \
    mid_max
from utils.analysis import get_pr_subset
from sklearn.inspection import partial_dependence


def get_pdp(pr):
    # Selected hyperparameters of the Random Forest model, based on the results
    # of "regression_both_test6.py" and "regression_both_test6_part1_plot.py"
    # Restrict the number of predictors to >= 1
    params = {'wd': {'n_estim': 400, 'n_features': 15,
                     'n_split': max_max},
              'wsi': {'n_estim': 100, 'n_features': 5,
                      'n_split': max_max}}
    max_lag = 1
    
    pred_list = subset_predictors(var, pr, params[var])
    input_met = load_met(path_out, 'cru', pr, max_lag)
    X = input_met.loc[:, pred_list]
        
    reg = load(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                            'part2', 'model', 'fit_' + var + '_' + \
                            pr + '.joblib'))

    ind = np.argsort(reg.feature_importances_)[::-1]

    pdp = np.full([100, 5], np.nan)
    xvals = np.full([100, 5], np.nan)
    for ii in range(5):
        pdp_temp, axes_temp = partial_dependence(reg, X.values, ind[ii])
        pdp[:len(pdp_temp[0,:]), ii] = pdp_temp[0, :]
        xvals[:len(axes_temp[0]), ii] = axes_temp[0]
    return pdp, xvals, pred_list[ind]


def plot_pdp(pdp, xvals, pred_list):
    fig, axes = plt.subplots(3, 2, figsize = (6.5, 6.5))
    for ii in range(5):
        ax = axes.flat[ii]
        ax.plot(xvals[:, ii], pdp[:, ii])
        ax.set_title(', '.join(pred_list[ii]))
    return fig, axes


mpl.rcParams['axes.titlesize'] = 5
mpl.rcParams['font.size'] = 5

# 'wsi', 
for var in ['wd']:
    province_subset = get_pr_subset(var)

    for pind, pr in enumerate(province_subset):
        pdp, xvals, pred_list2 = get_pdp(pr)
        fig, axes = plot_pdp(pdp, xvals, pred_list2)
        fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                 'part2', 'pdp', var + '_' + pr + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)
