"""
2022/10/30
ypwong1992@gmail.com
"""
import os
import numpy as np
import pandas as pd
from utils.constants import *
from utils.paths import *
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.gridspec import GridSpec
from utils.plotting import plot_province
import cartopy.crs as ccrs
from utils.plotting import ax_histogram
import itertools as it
#from scipy.stats import ttest_ind
from scipy.stats import mannwhitneyu


def read_data(var):
    goal = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part2', 'goal_' + var + '_bc.csv'),
                       index_col = 0)

    restored = {}
    for nn in range(npcs + 1):
        restored[nn] = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part4',
                                                'restored_' + var + '_' + str(nn) + '_bc.csv'), index_col = 0)

    le = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'part3', 'multivar_le.csv'),
                     parse_dates = True, index_col = 0, header = [0,1]).loc[:, (slice(None), '0')]
    le.columns = le.columns.droplevel(1)
    le = le.groupby(le.index.year, axis = 0).mean().loc[1920:, :]
    le_signs = {}
    for nn in range(npcs + 1):
        le_signs[(nn, 'positive')] = le.index[ le[str(nn)] > 0. ]
        le_signs[(nn, 'negative')] = le.index[ le[str(nn)] < 0. ]
    return goal, restored, le_signs


def read_sr(var, level):
    name_list = {
        'prov': ['Beijing', 'Tianjin', 'Hebei', 'Shanxi', 'Inner Mongolia', 'Liaoning', 'Jilin', 'Heilongjiang', 'Shanghai', 'Jiangsu', 'Zhejiang', 'Anhui', 'Fujian', 'Jiangxi', 'Shandong', 'Henan', 'Hubei', 'Hunan', 'Guangdong', 'Guangxi', 'Hainan', 'Chongqing', 'Sichuan', 'Guizhou', 'Yunnan', 'Shaanxi', 'Gansu', 'Qinghai', 'Ningxia', 'Xinjiang'],
        'sector': ['Ag', 'CoalM', 'PetroGas', 'MetalM', 'NonM', 'Food', 'Tex', 'Cloth', 'Wood', 'PaperM', 'PetroRe', 'CheInd', 'NonP', 'Metallurgy', 'MetalP', 'GeSpe', 'TransE', 'ElectriEqui', 'EltectroEqui', 'Instru', 'OthManu', 'EleWaSu', 'GasWaSu', 'Con', 'TransS', 'WholR', 'Hot', 'LeaComS', 'SciR', 'OthSe']
    }
    if var == 'vwsr_export':
        data = pd.read_csv(os.path.join(path_out, 'risk_results', '20220505', 'Raw data for Fig. 4~9', '03 all transfer metrix (fig8~9)', 
                                        f'VT_{level}_2012.csv'), header = None).sum(axis = 1)
    elif var == 'vwsr_import':
        data = pd.read_csv(os.path.join(path_out, 'risk_results', '20220505', 'Raw data for Fig. 4~9', '03 all transfer metrix (fig8~9)', 
                                        f'VT_{level}_2012.csv'), header = None).sum(axis = 0)
    elif var == 'lwsr':
        data = pd.read_csv(os.path.join(path_out, 'risk_results', '20220505', 'Raw data for Fig. 4~9',
                                        f'lwsr_2012_{level}.csv'), index_col = 0).iloc[:, 0]

    data.index = name_list[level]
    return data


mpl.rcParams['font.size'] = 6.
mpl.rcParams['axes.titlesize'] = 6.
mpl.rcParams['axes.titleweight'] = 'bold'


npcs = 1


for var in ['wsi', 'wd']:
    goal, restored, le_signs = read_data(var)
    # pdf plot
    fig, axes = plt.subplots(6, 5, figsize = (12., 12.), sharex = False, sharey = False)
    fig.subplots_adjust(wspace = 0.35, hspace = 0.35)
    h2 = []
    h3 = []
    for pind, pr in enumerate(province):
        ax = axes.flat[pind]

        if np.std(goal.loc[:, pr].values) < 1e-8:
            ax.bar(goal.loc[:, pr].values[0], 10, color = '#c5c5c5', edgecolor = '#808080')
            ax.set_xlim([goal.loc[:, pr].values[0] - 2, goal.loc[:, pr].values[0] + 2])
        else:
            h1 = ax_histogram(ax, goal.loc[:, pr].values, goal.shape[0] // 10, dist = 'norm', show = 'both', 
                              args_hist = {'color': '#c5c5c5', 'edgecolor': '#808080', 'alpha': 0.3},
                              args_line = {'color': '#808080'})

        clist = [['#a50f15', '#fb6a4a'], ['#B1D4E0', '#145DA0']]

        for nn in range(npcs + 1):
            if np.std(restored[nn].loc[:, pr].values) < 1e-8:
                ax.bar(restored[nn].loc[:, pr].values[0], 10, color = clist[nn][0], edgecolor = clist[nn][1])
                ax.set_xlim([restored[nn].loc[:, pr].values[0] - 2, restored[nn].loc[:, pr].values[0] + 2])
            else:
                for mm,ss,ls in zip(range(2), ['positive', 'negative'], ['-','--']):
                    htemp = ax_histogram(ax, goal.loc[le_signs[(nn, ss)], pr].values, goal.shape[0] // 10,
                                         dist = 'norm', show = 'line',
                                         args_hist = {'color': clist[nn][0], 'edgecolor': clist[nn][1],
                                                      'alpha': 0.3, 'ls': ls},
                                         args_line = {'color': clist[nn][1], 'ls': ls})
                    if pind == 0:
                        #h2.append(htemp[0])
                        #h3.append(htemp[1])
                        h2.append(htemp[0])
        #h4 = ax.axvline(goal.loc[2012, pr], color = 'k')
        h4 = ax.scatter(goal.loc[2012, pr], ax.get_ylim()[1]/2, marker = '^', color = 'k', zorder = 3)

        ax.set_title(pr, pad = 2)
        ax.tick_params(axis = 'both', length = 2)

    ax.legend(h1 + h2 + h3 + [h4],
              ['Histogram MVs', 'Fitted MVs',
               'Fitted SVD 1+', 'Fitted SVD 1\u2212',
               'Fitted SVD 2+', 'Fitted SVD 2\u2212',
               'Observed 2012 value'], ncol = 4, loc = (-5, -0.5))

    fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'analyze', 'figure2_' + var + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)

    # pdf plot, individual provinces
    for pind, pr in enumerate(province):
        fig, ax = plt.subplots(figsize = (1.8, 1.6), sharex = False, sharey = False)

        if np.std(goal.loc[:, pr].values) < 1e-8:
            ax.bar(goal.loc[:, pr].values[0], 10, color = '#c5c5c5', edgecolor = '#808080')
            ax.set_xlim([goal.loc[:, pr].values[0] - 2, goal.loc[:, pr].values[0] + 2])
        else:
            h1 = ax_histogram(ax, goal.loc[:, pr].values, goal.shape[0] // 10, dist = 'norm', show = 'both', 
                              args_hist = {'color': '#c5c5c5', 'edgecolor': '#808080', 'alpha': 0.3},
                              args_line = {'color': '#808080'})

        clist = [['#a50f15', '#fb6a4a'], ['#B1D4E0', '#145DA0']]

        for nn in range(npcs + 1):
            if np.std(restored[nn].loc[:, pr].values) < 1e-8:
                ax.bar(restored[nn].loc[:, pr].values[0], 10, color = clist[nn][0], edgecolor = clist[nn][1])
                ax.set_xlim([restored[nn].loc[:, pr].values[0] - 2, restored[nn].loc[:, pr].values[0] + 2])
            else:
                for mm,ss,ls in zip(range(2), ['positive', 'negative'], ['-','--']):
                    htemp = ax_histogram(ax, goal.loc[le_signs[(nn, ss)], pr].values, goal.shape[0] // 10,
                                         dist = 'norm', show = 'line',
                                         args_hist = {'color': clist[nn][0], 'edgecolor': clist[nn][1],
                                                      'alpha': 0.3, 'ls': ls},
                                         args_line = {'color': clist[nn][1], 'ls': ls})
        #h4 = ax.axvline(goal.loc[2012, pr], color = 'k')
        h4 = ax.scatter(goal.loc[2012, pr], ax.get_ylim()[1]/2, marker = '^', color = 'k', zorder = 3)

        ax.set_title(pr, pad = 2)
        ax.tick_params(axis = 'both', length = 2)

        fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'analyze', f'figure2_{var}_{pr}.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)


# Maps
for var in ['wsi', 'wd']:
    fig, ax = plt.subplots(figsize = (8, 8), subplot_kw = {'projection': ccrs.PlateCarree()})
    if var == 'wsi':
        goal, _, _ = read_data(var)
        levels = np.linspace(0., 1., 21)
        plot_province(ax, goal.loc[2012, :], cmap = 'Reds', norm = mpl.colors.BoundaryNorm(levels, 256), orientation = 'horizontal')
    else:
        goal, _, _ = read_data(var)
        levels = np.arange(0.01, 0.095, 0.005)
        plot_province(ax, goal.loc[2012, :], cmap = 'Reds', norm = mpl.colors.BoundaryNorm(levels, 256), orientation = 'horizontal')

    fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'analyze', 'figure2_' + var + '_map.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


lab = 'abcdefg'
fig = plt.figure(figsize = (8, 8))
gs = GridSpec(3, 2, width_ratios = [1.5, 1], wspace = 0.05)
axes = np.empty([3, 2], dtype = object)
for i in range(3):
    axes[i, 0] = fig.add_subplot(gs[i, 0], projection = ccrs.PlateCarree())
    axes[i, 1] = fig.add_subplot(gs[i, 1])

for i, var in enumerate(['lwsr', 'vwsr_export', 'vwsr_import']):
    for j, level in enumerate(['prov', 'sector']):
        ax = axes[i, j]
        vector = read_sr(var, level)
        if level == 'prov':
            if var == 'vwsr_import':
                norm = mpl.colors.BoundaryNorm(np.linspace(0.5, 3, 11) * 1e10, 256)
            else:
                norm = mpl.colors.LogNorm(vmin = 1e8, vmax = 1e12)
            plot_province(ax, vector, 'Reds', norm = norm)
        else:
            ax.bar(range(len(vector)), vector)
            ax.set_xticks(range(len(vector)))
            if var in ['lwsr', 'vwsr_export']: 
                ax.set_yscale('log')
            if i == 2:
                ax.set_xticklabels(vector.index, rotation = 90)
            else:
                ax.set_xticklabels([])
            ax.set_xlim([-0.5, 29.5])

        if var == 'lwsr':
            title = var.upper()
        else:
            title = var.split('_')[0].upper() + '$^{' + var.split('_')[1] + '}$'
        if level == 'prov': 
            title = title + ', province'
        else:
            title = title + ', sector'
        ax.set_title(title)
 
        ax.text(0.92, 0.95, lab[i*2 + j], fontweight = 'bold', transform = ax.transAxes)

fig.savefig(os.path.join(path_out, 'risk_results', '20220505', 'Raw data for Fig. 4~9', '03 all transfer metrix (fig8~9)', 'figure2_' + var + '_map2.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)



# Test the statistical significance of the differences in WSI & WI_A between the +/- phases
significance = pd.DataFrame(index = province, columns = pd.MultiIndex.from_product([['wsi', 'wd'], range(npcs+1)]))
for var in ['wsi', 'wd']:
    goal, restored, le_signs = read_data(var)

    for pind, pr in enumerate(province):
        for nn in range(npcs + 1):
            if np.nanstd(restored[nn].loc[:, pr].values) > 1e-8:
                pos = goal.loc[le_signs[(nn, 'positive')], pr].values
                neg = goal.loc[le_signs[(nn, 'negative')], pr].values

                _, pval = mannwhitneyu(pos, neg)

                significance.loc[pr, (var, nn)] = pval
significance.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6', 'analyze', 'significance.csv'))