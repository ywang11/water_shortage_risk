"""
20200601

Plot the outcome of data_pop_interplate.ncl
"""
from utils.paths import *
from utils.constants import *
from utils.io import *
import xarray as xr
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from matplotlib.colors import LogNorm

hr = xr.open_dataset(os.path.join(path_out, 'pop_count_0.5.nc'))
val = hr['pop'].copy(deep = True)
hr.close()


fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()})
ax.coastlines()
ax.set_extent([68.56, 140.01, 12.93, 55.8])
cf = ax.contourf(val.lon, val.lat, val, cmap = 'Spectral',
                 norm = LogNorm(np.nanmin(val) + 1e-6, np.nanmax(val)))
plt.colorbar(cf, ax = ax)
fig.savefig(os.path.join(path_out, 'pop_count_0.5.png'), dpi = 600,
            bbox_inches = 'tight')
plt.close(fig)

