"""
20210323
ywang254@utk.edu

Fit the relationship between Water Stress Index & Water Intensity of Crops
  in Physical Units like in part2.

Instead of using the actual precipitation values, use the residuals after
  the removal of the effects of SST variability.
"""
import pandas as pd
import numpy as np
import os
from utils.paths import path_data, path_out
from utils.constants import *
from utils.io import load_training_data, load_met
import matplotlib.pyplot as plt
from scipy.stats import pearsonr
from time import time
import multiprocessing as mp
import itertools as it
from joblib import load
from regression_both_test6_part2 import min_max, mid_max, max_max, \
    subset_predictors
from analyze_test6_figure4 import get_pr_subset


def reshape_restored(pr):
    temp = restored.loc[:, (slice(None), pr)]
    temp.columns = temp.columns.droplevel(1)
    temp.index = pd.MultiIndex.from_arrays([temp.index.year,
        temp.index.month.map(dict(zip([3, 6, 9, 12],
                                      ['MAM','JJA','SON','DJF'])))])
    temp = temp.unstack()

    temp2 = pd.concat([temp] * (1 + max_lag), axis = 0)
    temp2.index = pd.MultiIndex.from_product([ \
        ['Lag ' + str(n) for n in range(max_lag+1)],  temp.index])
    for lag in range(1, max_lag+1):
        temp2.loc['Lag ' + str(lag), :] = \
            np.concatenate([np.full([lag, temp2.shape[1]], np.nan),
                            temp2.loc['Lag ' + str(lag),
                                      :].values[:(-lag), :]],
                           axis = 0)
    temp2.index = temp2.index.reorder_levels([1,0])
    temp2 = temp2.unstack()

    return temp2

    

# set up consistent with previous scripts
npcs = 2
max_lag = 1
params = {'wd': {'n_estim': 400, 'n_features': 15,
                 'n_split': max_max},
          'wsi': {'n_estim': 100, 'n_features': 5,
                  'n_split': max_max}}
for var, nn in it.product(['wsi', 'wd'], range(npcs)):
    province_subset = get_pr_subset(var)

    restored = pd.read_excel(os.path.join(path_out, 'regression',
                                          'wsi_wd_test6', 'part3',
                                          'multivar_separate_restored.xlsx'),
                             sheet_name = 'PC' + str(nn),
                             index_col = 0, header = [0,1],
                             engine = 'openpyxl')

    predicted = pd.DataFrame(np.nan,
                             index = restored.index.year.unique(),
                             columns = province)

    for pind, pr in enumerate(province_subset):
        pred_list = subset_predictors(var, pr, params[var])
        met_data = reshape_restored(pr).loc[:, pred_list]
        temp = np.isfinite(met_data).all(axis = 1)
        X = met_data.loc[temp, :].values

        reg = load(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                'part2', 'model', 'fit_' + var + '_' + \
                                pr + '.joblib'))

        pred = reg.predict(X)

        predicted.loc[temp, pr] = pred

    predicted.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                  'part4', 'restored_' + var + '_' + \
                                  str(nn) + '.csv'))

    # bias-correct using the fitted information
    original = pd.read_csv(os.path.join(path_out, 'regression',
                                        'wsi_wd_test6', 'part2',
                                        'goal_' + var + '.csv'),
                           index_col = 0)
    if var == 'wsi':
        for pr in province:
            predicted.loc[:, pr] = predicted.loc[:, pr] * WSI0[pr] / \
                original.loc[2012, pr]
            if all(np.isnan(predicted.loc[:, pr])):
                predicted.loc[:, pr] = predicted.loc[:, pr].fillna(WSI0[pr])
        predicted = predicted.clip(0,1)
    else:
        irr_area = pd.read_csv(os.path.join(path_out, 'prv_irr_luh2.csv'),
                               index_col = 0, header = 0).loc[2012, :]
        liv = pd.read_csv(os.path.join(path_out, 'prv_PLivWN.csv'),
                          index_col = 0, parse_dates = True)
        liv.index = liv.index.year.values
        liv = liv.loc[2012, :]
        for pr in province:
            # convert from thousand m3 per year per km2 to m3 per year per yuan
            predicted.loc[:, pr] = predicted.loc[:, pr] * irr_area.loc[pr] / \
                AgO[pr] / 10
            # total ag sector = irrigation + livestock
            predicted.loc[:, pr] += (liv.loc[pr] * 100 / AgO[pr])
            # scale by the actual values in 2012
            actual_2012 = WD0[pr] / AgO[pr] / 1e4

            original_2012 = original.loc[2012, pr] * irr_area.loc[pr] / \
                AgO[pr] / 10
            original_2012 += (liv.loc[pr] * 100 / AgO[pr])

            predicted.loc[:, pr] = predicted.loc[:, pr] * actual_2012 / \
                original_2012
            if all(np.isnan(predicted.loc[:, pr])):
                predicted.loc[:, pr] = predicted.loc[:, pr].fillna(actual_2012)
        predicted = predicted.clip(0)
    predicted.to_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                  'part4', 'restored_' + var + '_' + \
                                  str(nn) + '_bc.csv'))
