"""
20200202
ywang254@utk.edu

Regression relationship between climate indices and each province's
(1) Water Stress Index (the ratio of water withdrawal to total available 
                        surface water)
(2) Water Intensity of Crops in Physical Units (m3 per year per km2)
"""
import pandas as pd
import numpy as np
import os
from utils.paths import path_data, path_out
#import statsmodels.api as stats
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import AdaBoostRegressor
from sklearn.model_selection import cross_val_score


ymin = 1971
ymax = 2005


#
df = pd.read_csv(os.path.join(path_out, 'inx.csv'), index_col = 0,
                 parse_dates = True)
df = df.loc[(df.index.year.values >= ymin) & (df.index.year.values <= ymax),
            :]
df = df.resample('1Y').mean()


##############################################################################
# WI_{ag}: thousand m3/year/km2
##############################################################################
irr_wat = pd.read_csv(os.path.join(path_out, 'prv_PIrrWN.csv'), index_col = 0,
                      parse_dates = True)
irr_wat = irr_wat.loc[(irr_wat.index.year.values >= ymin) & \
                      (irr_wat.index.year.values <= ymax), :]
irr_area = pd.read_csv(os.path.join(path_out, 'prv_irr_area.csv'),
                       index_col = 0, parse_dates = True)

irr_wat_unit_area = irr_wat / irr_area * 1e3

for prv in irr_wat_unit_area.columns:

    # !!!!!!!!!!!!!!!!!!!!!!! Compare two regressors using
    # cross_val_score, multiple metrics.

    reg = LinearRegression(fit_intercept = True, normalize = True)
    cross_val_score(reg, 
                    np.concatenate([np.ones((df.shape[0],1)),
                                    df.values], axis = 1),
                    irr_wat_unit_area.loc[:, prv].values)
    reg = reg.fit()

    fig, axes = plt.subplots(3, 2, figsize = (6, 8))
    fig.subplots_adjust(wspace = 0.2, hspace = 0.3)
    for i in range(df.shape[1]):
        ax = axes.flat[i]
        ax.plot(df.values[:, i], irr_wat_unit_area.loc[:, prv], '.')
        ax.set_title(df.columns[i] + (' %.4f' % reg.params[i+1]) + \
                     (' p=%.2f' % reg.pvalues[i+1]))
    fig.savefig(os.path.join(path_out, 'wsi_wd_regression',
                             prv + '.png'), dpi = 600.)
    plt.close(fig)


##############################################################################
# WSI
##############################################################################
