"""
20190910
ywang254@utk.edu

Aggregate the WD to provincial level.

PIrrWN unit: million cubic meter per month -> sum to per year.
Irrigation water demand (WD) unit: thousand cubic meter per year per km2
"""
from utils.paths import path_data, path_out
import os
import xarray as xr
import pandas as pd
import numpy as np
from utils.constants import province


#water_use = ['PDomUse', 'PDomWW', 'PIndUse', 'PIndWW', 'PIrrWN', 'PIrrWW',
#             'PLivWN']


ymin = 1971
ymax = 2005
irr_area = pd.read_csv(os.path.join(path_out, 'prv_irr_area.csv'),
                       index_col = 0, parse_dates = True)

prv = xr.open_dataset(os.path.join(path_out, 'CHN_mask_0.5.nc'))
map_keys = dict([(x.split(': ')[1], int(x.split(': ')[0])) \
                 for x in prv.attrs['Key'].split('; ')])


time_range = pd.date_range('1960-01-01', '2014-12-31', freq = 'YS')
prv_wd = pd.DataFrame(data = np.nan, index = time_range,
                      columns = province)


for fn in ['PIrrWN', 'PLivWN']:
    data = xr.open_dataset(os.path.join(path_out, fn + '.nc'),
                           decode_times = True)
    var = data.groupby('time.year').sum('time')

    for p in province:
        prv_wd.loc[:, p] = var['var'].where(np.abs(prv.region - \
                                                   map_keys[p]) < 1e-6 \
        ).sum(dim = ['lat', 'lon'])
    data.close()

    # save irrigation water demand to file
    prv_wd.to_csv(os.path.join(path_out, 'prv_' + fn + '.csv'))

    # Convert to unit area demand and save to file
    if fn == 'PIrrWN':
        wd = prv_wd.loc[(prv_wd.index.year.values >= ymin) & \
                        (prv_wd.index.year.values <= ymax), :] / \
                        irr_area * 1e3
        wd.to_csv(os.path.join(path_out, 'prv_' + fn + '_wd.csv'))

prv.close()
