#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=1,walltime=10:00:00,feature=beacon_gpu

cd $PBS_O_WORKDIR

cd ~/Git/water_shortage_risk

ipython -i regression_both_test6_part3_step1_test.py
