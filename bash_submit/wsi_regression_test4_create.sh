create=0
if [ ${create} -eq 1 ]
then
    for m in gfdl-esm2m hadgem2-es ipsl-cm5a-lr miroc-esm-chem noresm1-m
    do
	for p in 10 20 30
	do
	    cat wsi_regression_test4_template.sh | sed s/REPLACE1/${m}/ | sed s/REPLACE2/${p}/ > wsi_regression_test4_${m}_${p}.sh
	    cat ../wsi_regression_test4.py | sed s/REPLACE1/${m}/ | sed s/REPLACE2/${p}/ > ../wsi_regression_test4_${m}_${p}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for m in gfdl-esm2m hadgem2-es ipsl-cm5a-lr miroc-esm-chem noresm1-m
    do
	for p in 10 20 30
	do
	    rm wsi_regression_test4_${m}_${p}.sh
	    rm ../wsi_regression_test4_${m}_${p}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for m in gfdl-esm2m hadgem2-es ipsl-cm5a-lr miroc-esm-chem noresm1-m
    do
	for p in 10 20 30
	do
	    qsub wsi_regression_test4_${m}_${p}.sh
	done
    done
fi
