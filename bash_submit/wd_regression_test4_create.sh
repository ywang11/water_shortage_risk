create=0
if [ ${create} -eq 1 ]
then
    for p in 10 20 30
    do
	cat wd_regression_test4_template.sh | sed s/REPLACE1/${p}/ > wd_regression_test4_${p}.sh
	cat ../wd_regression_test4.py | sed s/REPLACE1/${p}/ > ../wd_regression_test4_${p}.py
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for p in 10 20 30
    do
	rm wd_regression_test4_${p}.sh
	rm ../wd_regression_test4_${p}.py
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for p in 10 20 30
    do
	qsub wd_regression_test4_${p}.sh
    done
fi
