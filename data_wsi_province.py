"""
20190910
ywang254@utk.edu

Aggregate the WSI to provincial level.

Unit: None
"""
from utils.constants import *
from utils.paths import path_data, path_out
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it


#scenario = ['hist_hist_1971_2004'] # , 'ssp2_rcp6p0_2005_2099'
s = 'hist_hist_1971_2004'

prv_wsi = pd.DataFrame(data = np.nan, index = wsi_period,
                       columns = pd.MultiIndex.from_product([province,model]))


prv = xr.open_dataset(os.path.join(path_out, 'CHN_mask_0.5.nc'))
map_keys = dict([(x.split(': ')[1], int(x.split(': ')[0])) \
                 for x in prv.attrs['Key'].split('; ')])

#for m,s in it.product(model, scenario):
for m in model:
    data = xr.open_dataset(os.path.join(path_data, 'WSI', 
                                        'wsi_' + m + '_' + s + '.nc4'),
                           decode_times = False)
    units, reference_date = data.time.attrs['units'].split('since')
    data['time'] = pd.date_range(start = reference_date, 
                                 periods = data.sizes['time'], freq = 'YS')
    wsi = data['wsi'][:, ::-1, :] # Reverse lat S->N

    overlap_time = data.time.indexes['time']
    overlap_time = overlap_time[(overlap_time >= wsi_period[0]) & \
                                (overlap_time <= wsi_period[-1])]

    for p in province:
        prv_wsi.loc[overlap_time, (p,m)] = \
            wsi.where(np.abs(prv.region-map_keys[p]) < 1e-6 \
            ).loc[overlap_time, :, :].mean(dim = ['lat', 'lon'])
    data.close()

prv_wsi.to_csv(os.path.join(path_out, 'prv_wsi.csv'))

prv.close()
