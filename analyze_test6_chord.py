"""
Select the most “average” years:
1. The WSI and WDA values were converted to ranks.
2. The mean squared distance from the average rank (50.5 year) over all the provinces was calculated for each individual year.
3. The mean squared distance was ranked again, to find the years that have relatively small mean squared distances.
4. The selected “average” years had ranks <= 20 in the mean squared distances of both the WSI and WDA.
"""
import os
import numpy as np
import pandas as pd
from utils.constants import *
from utils.paths import *
import itertools as it
from scipy.stats import rankdata

#
anchor = 50.5
npcs = 2

#
goal_middleness = pd.DataFrame(index = range(1920, 2020), columns = ['wsi', 'wd'])
restored_middleness = {}
for nn in range(npcs):
    restored_middleness[nn] = pd.DataFrame(index = range(1920, 2020), columns = ['wsi', 'wd'])

#
for var in ['wsi', 'wd']:
    #
    goal = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                    'part2', 'goal_' + var + '_bc.csv'),
                       index_col = 0)
    goal_middleness.loc[:, var] = np.mean(np.power(rankdata(goal.values, axis = 0) - anchor,
                                                   2),
                                          axis = 1)

    #
    for nn in range(npcs):
        temp = pd.read_csv(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                        'part4', 'restored_' + var + '_' + \
                                        str(nn) + '_bc.csv'),
                           index_col = 0).iloc[1:, :]
        restored_middleness[nn].loc[:, var] = np.mean(np.power(rankdata(temp.values,
                                                                        axis = 0) - anchor,
                                                               2), axis = 1)

goal_middleness_rank = pd.DataFrame(rankdata(goal_middleness.values, axis = 0),
                                    index = goal_middleness.index,
                                    columns = goal_middleness.columns)
goal_middleness_rank.to_csv(os.path.join(path_out, 'risk_results',
                                         'goal_middleness_rank.csv'))
for nn in range(npcs):
    restored_middleness_rank = pd.DataFrame(rankdata(restored_middleness[nn].values, axis = 0),
                                            index = restored_middleness[nn].index,
                                            columns = restored_middleness[nn].columns)
    restored_middleness_rank.to_csv(os.path.join(path_out, 'risk_results',
                                                 'restored_middleness_rank_' + str(nn) + '.csv'))

