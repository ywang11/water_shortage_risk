"""
2021/2/25
ywang254@utk.edu

Because univariate analysis shows that the MCA patterns associated with
 different meteorological patterns, (mainly P v.s. PET v.s. T, not 
 so much between Tmax, Tmin, Tavg), conduct the MCA between the SST
 and juxtaposed meteorological variables.
"""
import pandas as pd
import numpy as np
import os
from utils.io import load_met
from utils.paths import path_data, path_out
from utils.constants import *
from utils.plotting import plot_province
from utils.analysis import detrend, deseasonalize
import xarray as xr
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import gridspec
import cartopy.crs as ccrs
from time import time
import itertools as it
from warnings import filterwarnings
from xMCA import xMCA
from cartopy.util import add_cyclic_point
from dateutil.relativedelta import relativedelta
from regression_both_test6_part3_analysis_univariate \
    import get_met_data, get_sst, reshape_sst, restore_sst, match_time


#filterwarnings(action = 'error', category = FutureWarning)
#filterwarnings(action = 'ignore')

def plot_lh(lag):
    le = pd.read_csv(os.path.join(path_out, 'regression',
                                      'wsi_wd_test6', 'part3',
                                      'multivar_le.csv'),
                     index_col = 0, parse_dates = True,
                     header = [0,1]).loc[:, (slice(None), str(lag))]
    le.columns = le.columns.droplevel(1)

    hr = xr.open_dataset(os.path.join(path_out, 'regression',
                                      'wsi_wd_test6', 'part3',
                                      'multivar_lag_' + str(lag) + '_fit.nc'))
    lp = hr['leftPattern'].copy(deep = True)
    lh = hr['leftHomoPatterns'].copy(deep = True)
    hr.close()
    
    fig = plt.figure(figsize = (6.5, 6.5))
    gs = gridspec.GridSpec(npcs, 3)
    axes = np.empty(shape = (npcs, 3), dtype = object)
    for n in range(npcs):
        if n == 0:
            axes[n, 0] = fig.add_subplot(gs[n,0])
        else:
            axes[n, 0] = fig.add_subplot(gs[n,0], sharex = axes[0,0],
                                          sharey = axes[0,0])
        axes[n,1] = fig.add_subplot(gs[n,1],
            projection = ccrs.PlateCarree(central_longitude = 180.))
        axes[n,2] = fig.add_subplot(gs[n,2],
            projection = ccrs.PlateCarree(central_longitude = 180.))
    
    for n in range(npcs):
        ax = axes[n, 0]
        ax.plot(le.index, le.loc[:, str(n)], '-')
        if n == 0:
            ax.set_title('Expansion Coefs.')
        ax.set_ylabel('PC ' + str(n))
    
        ax = axes[n, 1]
        ax.coastlines()
        cf = ax.contourf(hr['lon'], hr['lat'],
                         lp.sel(n = n),
                         cmap = 'RdBu_r', transform = ccrs.PlateCarree(),
                         levels = np.linspace(-1, 1, 41), extend = 'both')
        if n == 0:
            ax.set_title('SST homogeneous pattern')
        plt.colorbar(cf, ax = ax, shrink = 0.7,
                     orientation = 'horizontal')
    
        ax = axes[n, 2]
        ax.coastlines()
        cf = ax.contourf(hr['lon'], hr['lat'], lh.sel(n = n),
                         cmap = 'RdBu_r', transform = ccrs.PlateCarree(),
                         levels = np.linspace(-1, 1, 41), extend = 'both')
        if n == 0:
            ax.set_title('SST heterogeneous pattern')            
        plt.colorbar(cf, ax = ax, shrink = 0.7,
                     orientation = 'horizontal')

    return fig, axes


def plot_rhet(lag):
    hr = xr.open_dataset(os.path.join(path_out, 'regression',
                                      'wsi_wd_test6', 'part3',
                                      'multivar_lag_' + str(lag) + '_fit.nc'))
    rhet = hr['rightHeteroPatterns'].copy(deep = True)
    rphet = hr['rightPval'].copy(deep = True)
    hr.close() 

    fig, axes = plt.subplots(figsize = (6.5, 6.5),
                             nrows = len(met), ncols = npcs,
                             subplot_kw = {'projection': ccrs.PlateCarree()})

    for n in range(npcs):
        for vind, var in enumerate(met):
            ax = axes[vind, n]
            ax.coastlines()
            ax.set_extent([67.1,136.2,7.7,53.9])

            temp = rhet.sel(n = n, var = var \
            ).where(rphet.sel(n = n, var = var) <= 0.05)
            if sum(~np.isnan(temp)) == 0:
                continue
            cf = plot_province(ax, temp,
                               levels = np.linspace(-.8, .8, 41),
                               cmap = 'Spectral', extend = 'both')
            if vind == 0:
                ax.set_title('PC ' + str(n))
            if n == 0:
                ax.text(-0.3, 0.5, 'Het. pattern ' + var + ', p<=0.05',
                        rotation = 90., transform = ax.transAxes,
                        verticalalignment = 'center')
            plt.colorbar(cf, ax = ax, shrink = 1.,
                         orientation = 'horizontal')

    return fig, axes


def plot_rh(lag):
    hr = xr.open_dataset(os.path.join(path_out, 'regression',
                                      'wsi_wd_test6', 'part3',
                                      'multivar_lag_' + str(lag) + '_fit.nc'))
    rh = hr['rightHomoPatterns'].copy(deep = True)
    hr.close() 

    fig, axes = plt.subplots(figsize = (6.5, 6.5),
                             nrows = len(met), ncols = npcs,
                             subplot_kw = {'projection': ccrs.PlateCarree()})

    for n in range(npcs):
        for vind, var in enumerate(met):
            ax = axes[vind, n]
            ax.coastlines()
            ax.set_extent([67.1,136.2,7.7,53.9])

            cf = plot_province(ax, rh.sel(n = n, var = var),
                               levels = np.linspace(-0.8, 0.8, 41),
                               cmap = 'Spectral', extend = 'both')
            if vind == 0:
                ax.set_title('PC ' + str(n))
            if n == 0:
                ax.text(-0.3, 0.5, 'Homo. pattern ' + var + ', p<=0.05',
                        rotation = 90., transform = ax.transAxes,
                        verticalalignment = 'center')
            plt.colorbar(cf, ax = ax, shrink = 1.,
                         orientation = 'horizontal')

    return fig, axes




if __name__ == '__main__':

    mpl.rcParams['font.size'] = 5
    mpl.rcParams['axes.titlesize'] = 5
    
    sst0 = get_sst()
    input_met0 = get_met_data(normalize = True, logpr = True)
    
    npcs = 5 # The number of PCs to use in MCA; total > 80%
    max_lag = 3 # since four seasons, consider effect < 1 year    
    
    # Save the heterogeneous correlations & p-values.
    frac_collection = pd.DataFrame(np.nan,
                                   index = range(npcs),
                                   columns = range(1 + max_lag))
    le_collection = pd.DataFrame(np.nan,
                                 index = input_met0.index,
        columns = pd.MultiIndex.from_product([range(npcs),
                                              range(1 + max_lag)],
                                             names = ['PC', 'Lag']))
    re_collection = pd.DataFrame(np.nan,
                                 index = input_met0.index,
        columns = pd.MultiIndex.from_product([range(npcs),
                                              range(1 + max_lag)],
                                             names = ['PC', 'Lag']))
    for lag in range(0, max_lag + 1):
        input_sst, input_met = match_time(sst0, input_met0, lag)

        print(input_sst['time'])
    
        input_sst, sst_trend = detrend(input_sst)
        input_sst, sst_seasonality = deseasonalize(input_sst)

        # Ensured that the rearrangement did not change the order of
        # province or met. vars.
        input_met = xr.DataArray(input_met.values.reshape(-1,
                                                          len(met),
                                                          len(province)),
                                 dims = ['time', 'var', 'province'],
                                 coords = {'time': input_met.index,
                                           'var': input_met.columns.levels[0],
                                           'province': range(len(province))})
        input_met, met_trend = detrend(input_met)
        input_met, met_seasonality = deseasonalize(input_met)

        # MCA
        # https://github.com/Yefee/xMCA
        # https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2005GL022800
        # (Sect 2.2)
        sst_ts = xMCA(input_sst, input_met)
        sst_ts.solver()
        lp, rp = sst_ts.patterns(n = npcs)
        le, re = sst_ts.expansionCoefs(n = npcs)
        
        frac = sst_ts.covFracs(n = npcs)
        print(frac)
        frac_collection.loc[:, lag] = frac.values
        
        lh, rh = sst_ts.homogeneousPatterns(n = npcs)
        _, rhet, _, rphet = sst_ts.heterogeneousPatterns(n = npcs, 
            statistical_test = True)
        
        for n in range(npcs):
            le_collection.loc[input_met['time'].to_index(),
                              (n, lag)] = le[n, :].values
            re_collection.loc[input_met['time'].to_index(),
                              (n, lag)] = re[n, :].values
    
        xr.Dataset({'sst': input_sst,
                    'sst_trend': sst_trend,
                    'sst_seasonality': sst_seasonality} \
                   ).to_netcdf(os.path.join(path_out, 'regression',
                                            'wsi_wd_test6', 'part3',
                                            'multivar_lag_' + str(lag) + \
                                            '_sst.nc'))
        xr.Dataset({'met': input_met,
                    'met_trend': met_trend,
                    'met_seasonality': met_seasonality} \
        ).to_netcdf(os.path.join(path_out, 'regression',
                                 'wsi_wd_test6', 'part3',
                                 'multivar_lag_' + str(lag) + \
                                 '_met.nc'))
        xr.Dataset({'leftPattern': lp,
                    'leftHomoPatterns': lh,
                    'rightPattern': rp,
                    'rightHomoPatterns': rh,
                    'rightHeteroPatterns': rhet,
                    'rightPval': rphet} \
        ).to_netcdf(os.path.join(path_out, 'regression',
                                 'wsi_wd_test6', 'part3',
                                 'multivar_lag_' + str(lag) + '_fit.nc'))
    frac_collection.to_csv(os.path.join(path_out, 'regression',
                                        'wsi_wd_test6', 'part3',
                                        'multivar_frac.csv'))
    le_collection.to_csv(os.path.join(path_out, 'regression',
                                      'wsi_wd_test6', 'part3',
                                      'multivar_le.csv'))
    re_collection.to_csv(os.path.join(path_out, 'regression',
                                      'wsi_wd_test6', 'part3',
                                      'multivar_re.csv'))

    # Plot the diagnostics
    for lag in range(0, max_lag + 1):
        fig, axes = plot_lh(lag)
        fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                 'part3', 'multivar_lag_' + str(lag) + \
                                 '_lh.png'), dpi = 600.,
                    bbox_inches = 'tight')
        plt.close(fig)

        fig, axes = plot_rhet(lag)
        fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                 'part3', 'multivar_lag_' + str(lag) + \
                                 '_rhet.png'), dpi = 600.,
                    bbox_inches = 'tight')
        plt.close(fig)

        fig, axes = plot_rh(lag)
        fig.savefig(os.path.join(path_out, 'regression', 'wsi_wd_test6',
                                 'part3', 'multivar_lag_' + str(lag) + \
                                 '_rh.png'), dpi = 600.,
                    bbox_inches = 'tight')
        plt.close(fig)
